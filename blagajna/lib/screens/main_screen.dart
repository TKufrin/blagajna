import 'dart:io';

import 'package:blagajna/backup_utils/backup_service.dart';
import 'package:blagajna/backup_utils/disk_service.dart';
import 'package:blagajna/backup_utils/google_drive_service.dart';
import 'package:blagajna/database.dart';
import 'package:blagajna/main.dart';
import 'package:blagajna/models/gdrive_copy.dart';
import 'package:blagajna/models/local_copy.dart';
import 'package:blagajna/screens/login.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:blagajna/app_state.dart';
import 'package:window_manager/window_manager.dart';

class MainScreenWidget extends StatefulWidget {
  const MainScreenWidget({Key? key}) : super(key: key);

  @override
  State<MainScreenWidget> createState() => _MainScreenWidgetState();
}

class _MainScreenWidgetState extends State<MainScreenWidget>
    with WindowListener {
  @override
  void initState() {
    windowManager.addListener(this);
    _init();
    super.initState();
  }

  @override
  void dispose() {
    windowManager.removeListener(this);
    super.dispose();
  }

  void _init() async {
    // Add this line to override the default close handler
    await windowManager.setPreventClose(true);
    setState(() {});
  }

  @override
  void onWindowClose() async {
    bool _isPreventClose = await windowManager.isPreventClose();
    if (_isPreventClose) {
      BackupService backupService = BackupService();
      await showDialog(
          context: context,
          builder: (context) {
            return SimpleDialog(
              title: const Text("Stvaranje sigurnosnih kopija..."),
              children: [
                FutureBuilder(
                  future: backupService.persistOnEndOfWork(),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      windowManager.destroy();
                    }
                    return const CircularProgressIndicator();
                  },
                )
              ],
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    AppState appState = Provider.of<AppState>(context, listen: true);
    return Scaffold(
        appBar: AppBar(
          title:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text(appState.onScreenWidgetTitle ?? ''),
            Text('${appState.employee!.name} ${appState.employee!.surname}')
          ]),
        ),
        body: appState.onScreenWidget,
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              /* const DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.blue,
                ),
                child: Text('Drawer Header'),
              ), */
              ListTile(
                title: const Text('Novi račun (F1)'),
                onTap: () {
                  setNewBillWidgetOnScreen(appState);
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title: const Text('Pregled stanja (F2)'),
                onTap: () {
                  setStockWidgetOnScreen(appState);
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title: const Text('Izlazni računi (F3)'),
                onTap: () {
                  setOutgoingBillsWidgetOnScreen(appState);
                  Navigator.pop(context);
                },
              ),
              ExpansionTile(
                title: const Text('Djelatnici'),
                children: <Widget>[
                  ListTile(
                    title: Row(children: const [
                      Spacer(flex: 1),
                      Expanded(flex: 15, child: Text('Pregled djelatnika (F4)'))
                    ]),
                    onTap: () async {
                      await setEmployeeDisplayWidgetOnScreen(appState);
                      Navigator.pop(context);
                    },
                  ),
                  ListTile(
                    title: Row(children: const [
                      Spacer(flex: 1),
                      Expanded(flex: 15, child: Text('Novi djelatnik (F5)'))
                    ]),
                    onTap: () {
                      setCreateEmployeeWidgetOnScreen(appState);
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
              ListTile(
                title: const Text('Zaključak kase (F6)'),
                onTap: () {
                  setRegisterLockWidgetOnScreen(appState);
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title: const Text('Postavke (F7)'),
                leading: const Icon(Icons.settings),
                onTap: () {
                  setSettingsWidgetOnScreen(appState);
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title: const Text('Odjava'),
                leading: const Icon(Icons.logout),
                onTap: () {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                        builder: (context) => LoginWidget(),
                      ),
                      (route) => false);
                },
              ),
            ],
          ),
        ));
  }
}
