import 'package:blagajna/app_state.dart';
import 'package:blagajna/components/text_styles/text_styles.dart';
import 'package:blagajna/screens/main_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

class DepositActivation extends StatefulWidget {
  const DepositActivation({Key? key}) : super(key: key);

  @override
  _DepositActivationState createState() {
    return _DepositActivationState();
  }
}

class _DepositActivationState extends State<DepositActivation> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _dateEditingController = TextEditingController();
  final TextEditingController _depositEditingController =
      TextEditingController();
  DateTime selectedDate = DateTime.now();
  double deposit = 500.0;

  final TextStyle _formLabelTextStyle = const TextStyle(
      fontSize: 30, fontWeight: FontWeight.bold, color: Colors.black);

  set _dateInForm(DateTime date) {
    setState(() {
      selectedDate = date;
      _dateEditingController.text =
          DateFormat("dd.MM.yyyy").format(selectedDate);
    });
  }

  @override
  void initState() {
    super.initState();
    _dateEditingController.text =
        DateFormat("dd.MM.yyyy").format(DateTime.now());
    _depositEditingController.text = deposit.toStringAsFixed(2);
  }

  @override
  void dispose() {
    _dateEditingController.dispose();
    _depositEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size formSize = const Size(500, 300);
    return Scaffold(
      body: Center(
        child: Form(
          key: _formKey,
          child: ConstrainedBox(
            constraints: BoxConstraints.tight(formSize),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  child: Text(
                    "Aktivacija kase",
                    style: _formLabelTextStyle,
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                          flex: 2,
                          child: TextFormField(
                            style: font30,
                            decoration: InputDecoration(
                              isDense: true,
                              border: const OutlineInputBorder(),
                              labelText: "Datum : ",
                              labelStyle: _formLabelTextStyle,
                            ),
                            controller: _dateEditingController,
                            enabled: false,
                          )),
                      Expanded(
                        flex: 1,
                        child: IconButton(
                          onPressed: () async {
                            final DateTime? picked = await showDatePicker(
                                context: context,
                                initialDate: DateTime.now(),
                                firstDate: DateTime(2015, 8),
                                lastDate: DateTime.now());
                            if (picked != null && picked != selectedDate) {
                              _dateInForm = picked;
                            }
                          },
                          splashRadius: 30.0,
                          splashColor: Colors.green,
                          iconSize: 40,
                          icon: const Icon(Icons.calendar_month),
                        ),
                      ),
                    ],
                  ),
                ),
                const Spacer(),
                Expanded(
                  flex: 2,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 2,
                        child: TextFormField(
                          style: font30,
                          decoration: InputDecoration(
                            isDense: true,
                            border: const OutlineInputBorder(),
                            labelText: "Polog kase (kn): ",
                            labelStyle: _formLabelTextStyle,
                          ),
                          controller: _depositEditingController,
                          validator: (String? value) {
                            if (value == null || value.isEmpty) {
                              return 'Upišite polog kase';
                            }
                            double? converted = double.tryParse(value);
                            if (converted == null ||
                                converted.compareTo(0.0) < 0) {
                              return "Polog mora biti pozitivan broj";
                            }
                            deposit = converted;
                            return null;
                          },
                        ),
                      ),
                      const Spacer(
                        flex: 1,
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  child: ElevatedButton(
                    autofocus: true,
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        AppState appState = context.read<AppState>();
                        appState.initialDeposit = deposit;
                        appState.date = selectedDate;
                        Navigator.pop(context);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const MainScreenWidget()),
                        );
                      }
                    },
                    child: Text(
                      'Potvrdi',
                      style: font30,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
