// ignore_for_file: must_be_immutable

import 'dart:async';

import 'package:blagajna/components/text_styles/text_styles.dart';
import 'package:blagajna/models/employee.dart';
import 'package:blagajna/models/notfoundexception.dart';
import 'package:flutter/material.dart';
import 'deposit_activation.dart';
import 'package:blagajna/app_state.dart';
import 'package:provider/provider.dart';

class LoginWidget extends StatelessWidget {
  LoginWidget({Key? key}) : super(key: key);
  final TextEditingController _fieldTextController = TextEditingController();
  late Size _textFieldSize;

  @override
  Widget build(BuildContext context) {
    _textFieldSize = const Size(300, 200);
    AppState appState = Provider.of<AppState>(context, listen: false);
    appState.init();
    return Scaffold(
      body: Center(
          child: ConstrainedBox(
        constraints: BoxConstraints.loose(_textFieldSize),
        child: TextField(
            style: font30,
            autofocus: true,
            decoration: const InputDecoration(
              hintText: 'Upišite PIN',
              focusColor: Colors.green,
            ),
            controller: _fieldTextController,
            obscureText: true,
            onSubmitted: ((String pin) async {
              if (pin.isEmpty) return;
              Employee emp = Employee();
              try {
                await emp.loadEmployeeByPIN(pin);
                AppState state = context.read<AppState>();
                state.employee = emp;
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const DepositActivation()),
                );
              } on NotFoundException {
                _fieldTextController.clear();
              }
            })),
      )),
    );
  }
}
