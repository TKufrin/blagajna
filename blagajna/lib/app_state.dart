import 'package:blagajna/models/company.dart';
import 'package:blagajna/models/notfoundexception.dart';

import 'models/employee.dart';
import 'package:flutter/material.dart';

class AppState extends ChangeNotifier {
  Employee? _employee;
  double? initialDeposit;
  DateTime? date;
  Widget? _onScreenWidget;
  String? _onScreenWidgetTitle;
  Company? _company;
  //AppState(this._employee, this.initialDeposit, this.date);

  void init() {
    _employee = null;
    initialDeposit = null;
    date = null;
    _onScreenWidget = null;
    _onScreenWidgetTitle = null;
    //company is loaded on app startup so we don't want to set it to null;
  }

  set employee(Employee? emp) {
    if (emp == null) return;
    _employee = emp;
    notifyListeners();
  }

  Employee? get employee {
    return _employee;
  }

  set company(Company? comp) {
    _company = comp;
    notifyListeners();
  }

  Company? get company {
    return _company;
  }

  Future<void> loadCompany() async {
    _company ??= Company();
    if (_company!.primaryKey == null) {
      //not loaded
      try {
        await _company!.load(1);
      } on NotFoundException {
        _company = null;
      }
    }
  }

  void setOnScreenWidget(Widget? newOnScreen, [String? onScreenWidgetTitle]) {
    _onScreenWidget = newOnScreen;
    _onScreenWidgetTitle = onScreenWidgetTitle;
    notifyListeners();
  }

  Widget? get onScreenWidget {
    return _onScreenWidget;
  }

  String? get onScreenWidgetTitle {
    return _onScreenWidgetTitle;
  }
}
