import 'package:blagajna/models/article.dart';

enum ArticleUnit {
  discrete("kom"),
  noUnit("nema"),
  weightKg("kg");

  const ArticleUnit(this.name);
  final String name;

  static ArticleUnit of(String name) {
    switch (name) {
      case "kom":
        return ArticleUnit.discrete;
      case "nema":
        return ArticleUnit.noUnit;
      case "kg":
        return ArticleUnit.weightKg;
      default:
        return ArticleUnit.noUnit;
    }
  }
}
