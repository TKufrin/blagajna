enum CopyFrequency {
  endOfWork("nakon završetka rada", 0),
  onReceiptIssue("nakon svakog izdanog računa", 1);

  const CopyFrequency(this.name, this.value);
  final String name;
  final int value;

  static CopyFrequency of(int value) {
    switch (value) {
      case 0:
        return CopyFrequency.endOfWork;
      case 1:
        return CopyFrequency.onReceiptIssue;
      default:
        return CopyFrequency.endOfWork;
    }
  }
}
