import 'package:blagajna/database.dart';
import 'package:blagajna/models/notfoundexception.dart';
import 'package:sqflite/sqflite.dart';

abstract class AbstractModel {
  int? primaryKey;
  late Map<String, dynamic> data;
  String getTable();
  String getPrimaryKeyName();
  List<String> getColumns();
  List<String> getNonNullableColumns() {
    return getColumns();
  }

  void emptyTheModel() {
    primaryKey = null;
    data = {};
  }

  Map<String, dynamic> toMap() {
    return data;
  }

  @override
  String toString() {
    return getTable() + toMap().toString();
  }

  AbstractModel.withData(Map<String, dynamic> row) {
    if (!row.keys.toSet().containsAll(getNonNullableColumns())) {
      throw Error();
    }
    data = {};
    for (String col in getColumns()) {
      if (row[col] != null) {
        data[col] = row[col];
      }
    }
    primaryKey = row[getPrimaryKeyName()];
  }

  AbstractModel() {
    data = {};
  }

  bool isLoaded() {
    return primaryKey != null;
  }

  bool equals(AbstractModel other) {
    return this.primaryKey == other.primaryKey;
  }

  Future<void> insert() async {
    if (primaryKey != null) return;
    Database db = await DatabaseHandler.instance.db;
    primaryKey = await db.insert(
      getTable(),
      data,
      conflictAlgorithm: ConflictAlgorithm.abort,
    );
  }

  Future<int> update() async {
    if (primaryKey == null) return 0;
    Database db = await DatabaseHandler.instance.db;
    return await db.update(
      getTable(),
      data,
      where: getPrimaryKeyName() + ' = ?',
      whereArgs: [primaryKey],
    );
  }

  Future<int> delete() async {
    if (primaryKey == null) return 0;
    Database db = await DatabaseHandler.instance.db;
    int count = await db.delete(
      getTable(),
      where: getPrimaryKeyName() + ' = ?',
      whereArgs: [primaryKey],
    );
    primaryKey = null;
    data = {};
    return count;
  }

  Future<void> load(int pk) async {
    Database db = await DatabaseHandler.instance.db;
    final List<Map<String, dynamic>> rows = await db.query(getTable(),
        where: getPrimaryKeyName() + ' = ?',
        whereArgs: [pk],
        columns: getColumns());

    if (rows.length != 1) {
      throw NotFoundException("Did not find row with pk = " + pk.toString());
    }

    data = rows[0];
    primaryKey = pk;
  }
}
