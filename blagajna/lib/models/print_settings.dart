import 'package:sqflite/sqflite.dart';
import 'package:blagajna/database.dart';
import 'package:blagajna/models/abstractmodel.dart';

class PrintSettings extends AbstractModel {
  static const String CREATE_TABLE =
      '''CREATE TABLE IF NOT EXISTS PrintSettings (
        idsetting INTEGER PRIMARY KEY,
        printerPort TEXT NOT NULL);''';

  PrintSettings.withData(Map<String, dynamic> row) : super.withData(row);

  PrintSettings();

  @override
  List<String> getColumns() {
    return ["printerPort"];
  }

  @override
  String getPrimaryKeyName() {
    return "idsetting";
  }

  @override
  String getTable() {
    return "PrintSettings";
  }

  String get printerPort {
    return data["printerPort"];
  }

  set printerPort(String port) {
    //DBs tend to return immutable maps
    data = Map.of(data);
    data["printerPort"] = port;
  }

  Future<void> updatePrintSettings() async {
    Database db = await DatabaseHandler.instance.db;
    int count = await db.update(
      getTable(),
      data,
      where: getPrimaryKeyName() + ' = ?',
      whereArgs: [1],
    );

    if (count == 0) {
      await db.insert(getTable(), data);
    }
  }
}
