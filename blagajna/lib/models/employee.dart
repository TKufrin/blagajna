import 'package:blagajna/models/abstractmodel.dart';
import 'package:blagajna/models/notfoundexception.dart';
import 'package:sqflite/sqflite.dart';
import 'package:blagajna/database.dart';

class Employee extends AbstractModel {
  static const String CREATE_TABLE = '''CREATE TABLE IF NOT EXISTS Employee (
        idemployee INTEGER PRIMARY KEY, 
        name TEXT NOT NULL, 
        surname TEXT NOT NULL, 
        pin TEXT NOT NULL UNIQUE, 
        oib TEXT NOT NULL UNIQUE, 
        admin BOOLEAN NOT NULL CHECK (admin IN (0, 1)));''';

  Employee.withData(Map<String, dynamic> row) : super.withData(row);

  Employee();

  @override
  List<String> getColumns() {
    return ["name", "surname", "pin", "oib", "admin"];
  }

  @override
  String getPrimaryKeyName() {
    return "idemployee";
  }

  @override
  String getTable() {
    return "Employee";
  }

  String get name {
    return data["name"];
  }

  String get surname {
    return data["surname"];
  }

  String get oib {
    return data["oib"];
  }

  int get admin {
    return data["admin"];
  }

  String get pin {
    return data["pin"];
  }

  set admin(int value) {
    data = Map.of(data);
    data["admin"] = value;
  }

  Future<void> loadEmployeeByPIN(String pin) async {
    Database db = await DatabaseHandler.instance.db;
    final List<Map<String, dynamic>> rows =
        await db.query(getTable(), where: 'pin = ?', whereArgs: [pin]);

    if (rows.length != 1) {
      throw NotFoundException("Did not find row with pin = " + pin);
    }

    Map<String, dynamic> modifyable = Map.of(rows[0]);

    int primaryKey = modifyable.remove(getPrimaryKeyName());
    data = modifyable;
    this.primaryKey = primaryKey;
  }

  Future<List<Employee>> selectAll() async {
    Database db = await DatabaseHandler.instance.db;
    final List<Map<String, dynamic>> rows = await db.query(getTable());

    return List.generate(rows.length, (i) {
      return Employee.withData(rows[i]);
    });
  }
}
