import 'package:blagajna/models/abstractmodel.dart';
import 'package:blagajna/models/enums/copy_frequency.dart';

abstract class AbstractCopy extends AbstractModel {
  AbstractCopy.withData(Map<String, dynamic> row) : super.withData(row);

  AbstractCopy();

  CopyFrequency get frequency {
    return CopyFrequency.of(data["frequency"]);
  }

  set frequency(CopyFrequency frequency) {
    data = Map.of(data);
    data["frequency"] = frequency.value;
  }

  int get enabled {
    return data["enabled"];
  }

  set enabled(int enabled) {
    data = Map.of(data);
    data["enabled"] = enabled;
  }
}
