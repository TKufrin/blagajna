import 'package:blagajna/database.dart';
import 'package:blagajna/models/abstract_copy.dart';
import 'package:blagajna/models/enums/copy_frequency.dart';
import 'package:blagajna/models/notfoundexception.dart';
import 'package:sqflite/sqflite.dart';

class LocalCopy extends AbstractCopy {
  static const String CREATE_TABLE = '''CREATE TABLE IF NOT EXISTS LocalCopy (
        idlocalcopy INTEGER PRIMARY KEY, 
        path TEXT NOT NULL UNIQUE, 
        frequency INTEGER NOT NULL, 
        enabled BOOLEAN NOT NULL CHECK (enabled IN (0, 1)));''';

  LocalCopy.withData(Map<String, dynamic> row) : super.withData(row);

  LocalCopy();

  @override
  List<String> getColumns() {
    return ["path", "frequency", "enabled"];
  }

  @override
  String getPrimaryKeyName() {
    return "idlocalcopy";
  }

  @override
  String getTable() {
    return "LocalCopy";
  }

  String get path {
    return data["path"];
  }

  Future<List<LocalCopy>> selectAll({String? orderBy}) async {
    Database db = await DatabaseHandler.instance.db;
    final List<Map<String, dynamic>> rows =
        await db.query(getTable(), orderBy: orderBy);

    return List.generate(rows.length, (i) {
      return LocalCopy.withData(rows[i]);
    });
  }

  Future<List<LocalCopy>> selectByFrequency(CopyFrequency frequency) async {
    Database db = await DatabaseHandler.instance.db;
    final List<Map<String, dynamic>> rows = await db.query(getTable(),
        where: "frequency = ?", whereArgs: [frequency.value]);

    return List.generate(rows.length, (i) {
      return LocalCopy.withData(rows[i]);
    });
  }

  Future<void> loadLocalCopyByExistingPath(String path) async {
    Database db = await DatabaseHandler.instance.db;
    final List<Map<String, dynamic>> rows =
        await db.query(getTable(), where: 'path = ?', whereArgs: [path]);

    if (rows.length != 1) {
      throw NotFoundException("Did not find row with path = " + path);
    }

    Map<String, dynamic> modifyable = Map.of(rows[0]);

    int primaryKey = modifyable.remove(getPrimaryKeyName());
    data = modifyable;
    this.primaryKey = primaryKey;
  }
}
