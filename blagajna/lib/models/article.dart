import 'package:blagajna/models/abstractmodel.dart';
import 'package:blagajna/models/enums/article_unit.dart';
import 'package:blagajna/models/notfoundexception.dart';
import 'package:sqflite/sqflite.dart';
import 'package:blagajna/database.dart';

class Article extends AbstractModel {
  static const String CREATE_TABLE = '''CREATE TABLE IF NOT EXISTS Article (
        idarticle INTEGER PRIMARY KEY,
        code INTEGER NOT NULL UNIQUE,
        barcode TEXT UNIQUE,
        unitPrice REAL NOT NULL,
        unit TEXT NOT NULL CHECK (unit IN ('kom', 'nema', 'kg')),
        onStock INTEGER,
        tax REAL,
        discount REAL,
        description TEXT NOT NULL UNIQUE);''';

  Article.withData(Map<String, dynamic> row) : super.withData(row);

  Article();

  @override
  List<String> getColumns() {
    return [
      "code",
      "barcode",
      "unitPrice",
      "unit",
      "onStock",
      "tax",
      "discount",
      "description"
    ];
  }

  @override
  List<String> getNonNullableColumns() {
    return ["code", "unitPrice", "unit", "description"];
  }

  @override
  String getPrimaryKeyName() {
    return "idarticle";
  }

  @override
  String getTable() {
    return "Article";
  }

  String? get barcode {
    return data["barcode"];
  }

  int get code {
    return data["code"];
  }

  double get unitPrice {
    return data["unitPrice"];
  }

  ArticleUnit get unit {
    return ArticleUnit.of(data["unit"]);
  }

  int? get onStock {
    return data["onStock"];
  }

  double? get tax {
    return data["tax"];
  }

  double? get discount {
    return data["discount"];
  }

  String get description {
    return data["description"];
  }

  Future<List<Article>> selectAll({String? orderBy, int? limit}) async {
    Database db = await DatabaseHandler.instance.db;
    final List<Map<String, dynamic>> rows =
        await db.query(getTable(), orderBy: orderBy, limit: limit);

    return List.generate(rows.length, (i) {
      return Article.withData(rows[i]);
    });
  }

  Future<void> loadArticleByCode(int code) async {
    Database db = await DatabaseHandler.instance.db;
    final List<Map<String, dynamic>> rows =
        await db.query(getTable(), where: 'code = ?', whereArgs: [code]);

    if (rows.length != 1) {
      throw NotFoundException(
          "Did not find row with code = " + code.toString());
    }

    Map<String, dynamic> modifyable = Map.of(rows[0]);

    int primaryKey = modifyable.remove(getPrimaryKeyName());
    data = modifyable;
    this.primaryKey = primaryKey;
  }

  Future<void> loadArticleByDescription(String description) async {
    Database db = await DatabaseHandler.instance.db;
    final List<Map<String, dynamic>> rows = await db
        .query(getTable(), where: 'description = ?', whereArgs: [description]);

    if (rows.length != 1) {
      throw NotFoundException("Did not find row with code = " + description);
    }

    Map<String, dynamic> modifyable = Map.of(rows[0]);

    int primaryKey = modifyable.remove(getPrimaryKeyName());
    data = modifyable;
    this.primaryKey = primaryKey;
  }

  Future<List<Article>> selectArticlesByPrefixOfDescription(String prefix,
      {String? orderBy, int? limit}) async {
    Database db = await DatabaseHandler.instance.db;
    final List<Map<String, dynamic>> rows = await db.query(getTable(),
        where: 'description LIKE ?',
        whereArgs: ['$prefix%'],
        orderBy: orderBy ?? getPrimaryKeyName(),
        limit: limit);

    return List.generate(rows.length, (i) {
      return Article.withData(rows[i]);
    });
  }

  Future<Article?> selectFirstArticleByPrefixOfDescription(
      String prefix) async {
    Database db = await DatabaseHandler.instance.db;
    final List<Map<String, dynamic>> rows = await db.query(getTable(),
        where: 'description LIKE ?',
        whereArgs: ['$prefix%'],
        orderBy: getPrimaryKeyName(),
        limit: 1);

    if (rows.isEmpty) return null;

    return Article.withData(rows[0]);
  }
}
