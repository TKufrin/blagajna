import 'package:blagajna/database.dart';
import 'package:blagajna/models/abstractmodel.dart';
import 'package:sqflite/sqflite.dart';

class Bill extends AbstractModel {
  static const String CREATE_TABLE = '''CREATE TABLE IF NOT EXISTS Bill (
        idbill INTEGER PRIMARY KEY, 
        issuedAt INTEGER NOT NULL UNIQUE DEFAULT (strftime('%s')),
        sum REAL NOT NULL, 
        idemployee INTEGER NOT NULL,
        FOREIGN KEY (idemployee) REFERENCES Employee(idemployee) ON UPDATE CASCADE ON DELETE RESTRICT);''';

  Bill.withData(Map<String, dynamic> data) : super.withData(data);

  Bill();

  @override
  List<String> getColumns() {
    return ['issuedAt', 'sum', 'idemployee'];
  }

  @override
  List<String> getNonNullableColumns() {
    //issuedAt has default value so it's not needed for peristance
    return ['sum', 'idemployee'];
  }

  @override
  String getPrimaryKeyName() {
    return "idbill";
  }

  @override
  String getTable() {
    return "Bill";
  }

  int get issuedAt {
    //seconds from start of epoch
    return data['issuedAt'];
  }

  double get sum {
    return data['sum'];
  }

  int get idemployee {
    return data['idemployee'];
  }

  Future<int> getNumberOfBillsInCurrentYear() async {
    Database db = await DatabaseHandler.instance.db;
    int unixTimestampToday = DateTime.now().millisecondsSinceEpoch ~/ 1000;
    final List<Map<String, dynamic>> rows = await db.query(getTable(),
        orderBy: "issuedAt DESC",
        where: "issuedAt <= ?",
        whereArgs: [unixTimestampToday]);
    return rows.length;
  }

  Future<List<Bill>> getBillsOnDate(DateTime date, {String? orderBy}) async {
    Database db = await DatabaseHandler.instance.db;

    int lowerBound =
        DateTime(date.year, date.month, date.day).millisecondsSinceEpoch ~/
            1000;
    int upperBound = DateTime(date.year, date.month, date.day)
            .add(const Duration(days: 1))
            .subtract(const Duration(seconds: 1))
            .millisecondsSinceEpoch ~/
        1000;
    final List<Map<String, dynamic>> rows = await db.query(getTable(),
        where: "issuedAt <= ? AND issuedAt >= ?",
        whereArgs: [upperBound, lowerBound],
        orderBy: orderBy);

    return List.generate(rows.length, (i) {
      return Bill.withData(rows[i]);
    });
  }

  Future<List<Bill>> selectAll({String? orderBy}) async {
    Database db = await DatabaseHandler.instance.db;

    final List<Map<String, dynamic>> rows =
        await db.query(getTable(), orderBy: orderBy);

    return List.generate(rows.length, (i) {
      return Bill.withData(rows[i]);
    });
  }
}
