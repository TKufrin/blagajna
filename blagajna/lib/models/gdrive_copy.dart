import 'package:blagajna/database.dart';
import 'package:blagajna/models/abstract_copy.dart';
import 'package:blagajna/models/enums/copy_frequency.dart';
import 'package:blagajna/models/notfoundexception.dart';
import 'package:sqflite/sqflite.dart';

class GDriveCopy extends AbstractCopy {
  static const String CREATE_TABLE = '''CREATE TABLE IF NOT EXISTS GDriveCopy (
        idgdrivecopy INTEGER PRIMARY KEY,
        uniqueName TEXT NOT NULL UNIQUE,
        tokenType TEXT NOT NULL,
        tokenData TEXT NOT NULL UNIQUE,
        tokenExpiry TEXT NOT NULL,
        refreshToken TEXT,
        frequency INTEGER NOT NULL, 
        enabled BOOLEAN NOT NULL CHECK (enabled IN (0, 1)));''';

  GDriveCopy.withData(Map<String, dynamic> row) : super.withData(row);

  GDriveCopy();

  @override
  List<String> getColumns() {
    return [
      "uniqueName",
      "tokenType",
      "tokenData",
      "tokenExpiry",
      "refreshToken",
      "frequency",
      "enabled"
    ];
  }

  @override
  List<String> getNonNullableColumns() {
    return [
      "uniqueName",
      "tokenType",
      "tokenData",
      "tokenExpiry",
      "frequency",
      "enabled"
    ];
  }

  @override
  String getPrimaryKeyName() {
    return "idgdrivecopy";
  }

  @override
  String getTable() {
    return "GDriveCopy";
  }

  String get uniqueName {
    return data["uniqueName"];
  }

  String get tokenType {
    return data["tokenType"];
  }

  set tokenType(String tokenType) {
    data = Map.of(data);
    data["tokenType"] = tokenType;
  }

  String get tokenData {
    return data["tokenData"];
  }

  set tokenData(String tokenData) {
    data = Map.of(data);
    data["tokenData"] = tokenData;
  }

  String get tokenExpiry {
    return data["tokenExpiry"];
  }

  set tokenExpiry(String tokenExpiry) {
    data = Map.of(data);
    data["tokenExpiry"] = tokenExpiry;
  }

  String? get refreshToken {
    return data["refreshToken"];
  }

  set refreshToken(String? refreshToken) {
    data = Map.of(data);
    data["refreshToken"] = refreshToken;
  }

  Future<List<GDriveCopy>> selectAll({String? orderBy}) async {
    Database db = await DatabaseHandler.instance.db;
    final List<Map<String, dynamic>> rows =
        await db.query(getTable(), orderBy: orderBy);

    return List.generate(rows.length, (i) {
      return GDriveCopy.withData(rows[i]);
    });
  }

  Future<List<GDriveCopy>> selectByFrequency(CopyFrequency frequency) async {
    Database db = await DatabaseHandler.instance.db;
    final List<Map<String, dynamic>> rows = await db.query(getTable(),
        where: "frequency = ?", whereArgs: [frequency.value]);

    return List.generate(rows.length, (i) {
      return GDriveCopy.withData(rows[i]);
    });
  }

  Future<void> loadGDriveCopyByUniqueName(String uniqueName) async {
    Database db = await DatabaseHandler.instance.db;
    final List<Map<String, dynamic>> rows =
        await db.query(getTable(), where: 'email = ?', whereArgs: [uniqueName]);

    if (rows.length != 1) {
      throw NotFoundException(
          "Did not find row with uniqueName = " + uniqueName);
    }

    Map<String, dynamic> modifyable = Map.of(rows[0]);

    int primaryKey = modifyable.remove(getPrimaryKeyName());
    data = modifyable;
    this.primaryKey = primaryKey;
  }
}
