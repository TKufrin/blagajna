import 'package:blagajna/database.dart';
import 'package:blagajna/models/abstractmodel.dart';
import 'package:sqflite/sqflite.dart';

class BillArticle extends AbstractModel {
  static const String CREATE_TABLE = '''CREATE TABLE IF NOT EXISTS BillArticle (
        idbillarticle INTEGER PRIMARY KEY, 
        idbill INTEGER NOT NULL, 
        idarticle INTEGER NOT NULL,
        count REAL NOT NULL, 
        discount REAL, 
        tax REAL, 
        unitPrice REAL NOT NULL,
        FOREIGN KEY (idbill) REFERENCES Bill(idbill) ON UPDATE CASCADE,
        FOREIGN KEY (idarticle) REFERENCES Article(idarticle) ON UPDATE CASCADE);''';

  BillArticle.withData(Map<String, dynamic> data) : super.withData(data);

  BillArticle();

  @override
  List<String> getColumns() {
    return ['idbill', 'idarticle', 'count', 'discount', 'tax', 'unitPrice'];
  }

  @override
  List<String> getNonNullableColumns() {
    return ['idbill', 'idarticle', 'count', 'unitPrice'];
  }

  @override
  String getPrimaryKeyName() {
    return "idbillarticle";
  }

  @override
  String getTable() {
    return "BillArticle";
  }

  int get idbill {
    return data['idbill'];
  }

  int get idarticle {
    return data['idarticle'];
  }

  double get count {
    return data['count'];
  }

  double? get discount {
    return data['discount'];
  }

  double? get tax {
    return data['tax'];
  }

  double get unitPrice {
    return data['unitPrice'];
  }

  Future<List<BillArticle>> getByBillId(int idbill) async {
    Database db = await DatabaseHandler.instance.db;

    final List<Map<String, dynamic>> rows =
        await db.query(getTable(), where: "idbill = ?", whereArgs: [idbill]);

    return List.generate(rows.length, (i) {
      return BillArticle.withData(rows[i]);
    });
  }
}
