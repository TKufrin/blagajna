import 'package:sqflite/sqflite.dart';
import 'package:blagajna/database.dart';
import 'package:blagajna/models/abstractmodel.dart';

class Company extends AbstractModel {
  static const String CREATE_TABLE = '''CREATE TABLE IF NOT EXISTS Company (
        idcompany INTEGER PRIMARY KEY, 
        name TEXT NOT NULL UNIQUE, 
        officeAddress TEXT NOT NULL, 
        headquartersAddress TEXT NOT NULL, 
        oib TEXT NOT NULL UNIQUE,
        owner TEXT NOT NULL);''';

  Company.withData(Map<String, dynamic> row) : super.withData(row);

  Company();

  @override
  List<String> getColumns() {
    return ["name", "officeAddress", "headquartersAddress", "oib", "owner"];
  }

  @override
  String getPrimaryKeyName() {
    return "idcompany";
  }

  @override
  String getTable() {
    return "Company";
  }

  String get name {
    return data["name"];
  }

  String get officeAddress {
    return data["officeAddress"];
  }

  String get oib {
    return data["oib"];
  }

  String get owner {
    return data["owner"];
  }

  String get headquartersAddress {
    return data["headquartersAddress"];
  }

  Future<void> updateCompanyData() async {
    Database db = await DatabaseHandler.instance.db;
    int count = await db.update(
      getTable(),
      data,
      where: getPrimaryKeyName() + ' = ?',
      whereArgs: [1],
    );

    if (count == 0) {
      await db.insert(getTable(), data);
    }
  }
}
