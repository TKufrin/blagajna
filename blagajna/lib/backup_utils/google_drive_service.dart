import 'dart:io';

import 'package:blagajna/database.dart';
import 'package:blagajna/models/gdrive_copy.dart';
import 'package:googleapis/drive/v3.dart' as gdrive;
import 'package:googleapis_auth/auth_io.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;

class GoogleDriveService {
  final List<String> _scopes = [gdrive.DriveApi.driveScope];
  final String _clientId =
      '29566069239-fu6p4msln97449pu09ek5p3kkmp39omj.apps.googleusercontent.com';
  final String _clientSecret = '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';

  Future<AuthClient> _getHttpClient(GDriveCopy gDriveCopy) async {
    //date time parses in UTC!!!
    if (DateTime.parse(gDriveCopy.tokenExpiry).isBefore(DateTime.now())) {
      AccessCredentials newCredentials = await refreshCredentials(
          ClientId(_clientId, _clientSecret),
          AccessCredentials(
              AccessToken(gDriveCopy.tokenType, gDriveCopy.tokenData,
                  DateTime.parse(gDriveCopy.tokenExpiry)),
              gDriveCopy.refreshToken,
              _scopes),
          http.Client());
      //save refreshed credentials
      gDriveCopy.tokenType = newCredentials.accessToken.type;
      gDriveCopy.tokenData = newCredentials.accessToken.data;
      gDriveCopy.tokenExpiry = newCredentials.accessToken.expiry.toString();
      gDriveCopy.refreshToken = newCredentials.refreshToken;
      await gDriveCopy.update();
      return authenticatedClient(http.Client(), newCredentials);
    } else {
      //Already authenticated
      return authenticatedClient(
          http.Client(),
          AccessCredentials(
              AccessToken(gDriveCopy.tokenType, gDriveCopy.tokenData,
                  DateTime.parse(gDriveCopy.tokenExpiry)),
              gDriveCopy.refreshToken,
              _scopes));
    }
  }

  void _prompt(String url) {
    launchUrl(Uri.parse(url));
  }

  Future<void> upload(File file, GDriveCopy gDriveCopy) async {
    var client = await _getHttpClient(gDriveCopy);
    try {
      var driveApi = gdrive.DriveApi(client);

      // Check if the folder exists. If it doesn't exist, create it and return the ID.
      final folderId = await _getFolderId(driveApi);
      if (folderId == null) {
        // not signed in
        //somebody changed something in db so credentials are invalid
        await gDriveCopy.delete();
        client.close();
        return;
      }

      gdrive.File fileMeta = gdrive.File();
      fileMeta.name = file.uri.pathSegments.last;
      fileMeta.modifiedTime = DateTime.now();

      final String? existingFileId =
          await _getFileId(driveApi, fileMeta.name!, folderId);

      if (existingFileId != null) {
        await driveApi.files.update(fileMeta, existingFileId,
            addParents: folderId,
            uploadMedia: gdrive.Media(file.openRead(), file.lengthSync()));
        print(existingFileId);
      } else {
        fileMeta.parents = [folderId];
        gdrive.File res = await driveApi.files.create(
          fileMeta,
          uploadMedia: gdrive.Media(file.openRead(), file.lengthSync()),
        );
        print('Response here ${res.toJson()}');
      }

      client.close();
    } on AccessDeniedException {
      // entered here if somebody changed database file
      //somebody changed something in db so credentials are invalid
      await gDriveCopy.delete();
    }
  }

  Future<String?> _getFolderId(gdrive.DriveApi driveApi) async {
    const mimeType = "application/vnd.google-apps.folder";
    String folderName = DatabaseHandler.getDbFolderName();

    try {
      final gdrive.FileList found = await driveApi.files.list(
        spaces: 'drive',
        corpora: 'user',
        q: "mimeType = '$mimeType' and name = '$folderName' and trashed = false",
        $fields: "files(id, name)",
      );
      final List<gdrive.File>? files = found.files;
      if (files == null) {
        //not signed in
        return null;
      }

      // The folder already exists
      if (files.isNotEmpty) {
        return files.first.id;
      }

      // Create a folder
      var folder = gdrive.File();
      folder.name = folderName;
      folder.mimeType = mimeType;
      final folderCreation = await driveApi.files.create(folder);
      print("Folder ID: ${folderCreation.id}");

      return folderCreation.id;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<String?> _getFileId(
      gdrive.DriveApi driveApi, String fileName, String parentFolderId) async {
    const mimeType = "application/vnd.google-apps.folder";
    //String fileName = DatabaseHandler.getDbName();

    try {
      final gdrive.FileList found = await driveApi.files.list(
        spaces: 'drive',
        corpora: 'user',
        q: "mimeType != '$mimeType' and name = '$fileName' and '$parentFolderId' in parents and trashed = false",
        $fields: "files(id, name)",
      );
      final List<gdrive.File>? files = found.files;
      if (files == null) {
        //not signed in
        return null;
      }

      // The file already exists
      if (files.isNotEmpty) {
        print('Found file ${files.first.toJson()}');
        return files.first.id;
      }
      return null;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<GDriveCopy> addNewGDriveAccount(String uniqueName) async {
    //date time parses in UTC!!!
    var authClient = await clientViaUserConsent(
        ClientId(_clientId, _clientSecret), _scopes, _prompt);
    var gdc = GDriveCopy.withData({
      "uniqueName": uniqueName,
      "tokenType": authClient.credentials.accessToken.type,
      "tokenData": authClient.credentials.accessToken.data,
      "tokenExpiry": authClient.credentials.accessToken.expiry.toString(),
      "refreshToken": authClient.credentials.refreshToken,
      "frequency": 0,
      "enabled": 0
    });
    authClient.close();
    return gdc;
  }
}
