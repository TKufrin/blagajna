import 'dart:io';
import 'package:blagajna/database.dart';

class DiskService {
  //LOOK AT permission_handler PACKAGE FOR ANDROID
  Future<void> backupDB(Directory toDir) async {
    //final dbFolder = (await getApplicationDocumentsDirectory()).path;
    final String dbPath = await DatabaseHandler.getDbPath();
    File source = File(dbPath);

    //Directory copyTo = Directory("storage/emulated/0/Sqlite Backup");
    //await copyTo.create();

    await toDir.create();

    String newPath = "${toDir.path}/${DatabaseHandler.getDbName()}";

    try {
      await source.copy(newPath);
    } on FileSystemException {
      //ON OS ERROR : ACCESS DENIED TODO
    }
  }

  Future<void> restoreDb() async {}
}
