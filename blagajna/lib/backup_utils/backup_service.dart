import 'dart:io';

import 'package:blagajna/backup_utils/disk_service.dart';
import 'package:blagajna/backup_utils/google_drive_service.dart';
import 'package:blagajna/database.dart';
import 'package:blagajna/models/enums/copy_frequency.dart';
import 'package:blagajna/models/gdrive_copy.dart';
import 'package:blagajna/models/local_copy.dart';

class BackupService {
  Future<void> persistOnEndOfWork() async {
    Iterable<LocalCopy> localCopies =
        (await LocalCopy().selectByFrequency(CopyFrequency.endOfWork))
            .where((element) => element.enabled == 1);
    Iterable<GDriveCopy> gdriveCopies =
        (await GDriveCopy().selectByFrequency(CopyFrequency.endOfWork))
            .where((element) => element.enabled == 1);
    DiskService ds = DiskService();
    GoogleDriveService gs = GoogleDriveService();
    for (final c in localCopies) {
      await ds.backupDB(Directory(c.path));
    }
    String dbPath = await DatabaseHandler.getDbPath();
    File db = File(dbPath);
    for (final c in gdriveCopies) {
      await gs.upload(db, c);
    }
  }

  Future<void> persistOnReciptIssue() async {
    Iterable<LocalCopy> localCopies =
        (await LocalCopy().selectByFrequency(CopyFrequency.onReceiptIssue))
            .where((element) => element.enabled == 1);
    Iterable<GDriveCopy> gdriveCopies =
        (await GDriveCopy().selectByFrequency(CopyFrequency.onReceiptIssue))
            .where((element) => element.enabled == 1);
    DiskService ds = DiskService();
    GoogleDriveService gs = GoogleDriveService();
    for (final c in localCopies) {
      await ds.backupDB(Directory(c.path));
    }
    String dbPath = await DatabaseHandler.getDbPath();
    File db = File(dbPath);
    for (final c in gdriveCopies) {
      await gs.upload(db, c);
    }
  }
}
