import 'dart:async';
import 'dart:math';

import 'package:blagajna/models/article.dart';
import 'package:blagajna/models/bill.dart';
import 'package:blagajna/models/bill_article.dart';
import 'package:blagajna/models/company.dart';
import 'package:blagajna/models/employee.dart';
import 'package:blagajna/models/enums/article_unit.dart';
import 'package:blagajna/models/gdrive_copy.dart';
import 'package:blagajna/models/local_copy.dart';
import 'package:blagajna/models/print_settings.dart';
import 'package:path/path.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io' show Platform;

class DatabaseHandler {
  static const String _dbName = 'blagajna_storage.db';
  static const int _dbVersion = 10;
  static const String _dbFolder = 'blagajna-db';
  static const List<String> _createTableStatements = [
    Employee.CREATE_TABLE,
    Company.CREATE_TABLE,
    Article.CREATE_TABLE,
    Bill.CREATE_TABLE,
    BillArticle.CREATE_TABLE,
    PrintSettings.CREATE_TABLE,
    LocalCopy.CREATE_TABLE,
    GDriveCopy.CREATE_TABLE
  ];

  DatabaseHandler._privateConstructor() {
    _initSQLiteSettings();
  }

  static final DatabaseHandler instance = DatabaseHandler._privateConstructor();

  late Database _database;
  bool _dbInitialized = false;

  Future<Database> get db async {
    if (_dbInitialized) return _database;
    _database = await _initDatabase();
    _dbInitialized = true;
    return _database;
  }

  void _initSQLiteSettings() {
    if (Platform.isWindows || Platform.isLinux) {
      // Initialize FFI
      sqfliteFfiInit();
      // Change the default factory. On iOS/Android, if not using `sqlite_flutter_lib` you can forget
      // this step, it will use the sqlite version available on the system.
      databaseFactory = databaseFactoryFfi;
    }
  }

  static Future<String> getDbPath() async {
    return join((await getApplicationDocumentsDirectory()).path,
        DatabaseHandler._dbFolder, DatabaseHandler._dbName);
  }

  static Future<String> getDbFolder() async {
    return join((await getApplicationDocumentsDirectory()).path,
        DatabaseHandler._dbFolder);
  }

  static String getDbName() {
    return _dbName;
  }

  static String getDbFolderName() {
    return _dbFolder;
  }

  Future<Database> _initDatabase() async {
    String path = await getDbPath();
    Database db = await openDatabase(path,
        version: DatabaseHandler._dbVersion,
        onCreate: _onCreate,
        onUpgrade: _onUpgrade);
    db.execute("PRAGMA foreign_keys = ON;");
    db.execute("PRAGMA encoding='UTF-8';");

    Map<String, dynamic> admin = {
      "name": "Tin",
      "surname": "Kufrin",
      "pin": "12345",
      "oib": "00025228072",
      "admin": 1,
    };
    Employee emp = Employee();
    await db.insert(emp.getTable(), admin,
        conflictAlgorithm: ConflictAlgorithm.ignore);
    //await _insertDummyArticles(db);
    return db;
  }

  Future<void> _onCreate(Database db, int version) async {
    for (String createTable in _createTableStatements) {
      await db.execute(createTable);
    }
  }

  //just temporary
  FutureOr<void> _onUpgrade(Database db, int oldVersion, int newVersion) async {
    for (String createTable in _createTableStatements) {
      await db.execute(createTable);
    }
  }

  Future<void> _insertDummyArticles(Database db) async {
    List<String> units = [
      ArticleUnit.weightKg.name,
      ArticleUnit.discrete.name,
      ArticleUnit.noUnit.name
    ];
    Random random = Random();
    int code = 7;
    double unitPrice = _doubleInRange(random, 100, 200);
    String unit = units[_doubleInRange(random, 1, 4).toInt()];
    double tax = _doubleInRange(random, 10, 30);
    double discount = _doubleInRange(random, 1, 10);
    String description = "usluga" + code.toString();
    db.transaction<void>((txn) async {
      for (int i = 0; i < 100; i++) {
        await txn.insert("Article", {
          'description': description,
          'tax': tax,
          'discount': discount,
          'unitPrice': unitPrice,
          'unit': unit,
          'code': code
        });
        code++;
        unitPrice = _doubleInRange(random, 100, 200);
        tax = _doubleInRange(random, 10, 30);
        discount = _doubleInRange(random, 1, 10);
        description = "usluga" + code.toString();
        unit = units[_doubleInRange(random, 0, 2).toInt()];
      }
    });
  }

  double _doubleInRange(Random source, int start, int end) => double.parse(
      (source.nextDouble() * (end - start) + start).toStringAsFixed(2));
}
