import 'package:blagajna/components/new_bill.dart';
import 'package:blagajna/models/article.dart';
import 'package:blagajna/models/bill.dart';
import 'package:blagajna/models/bill_article.dart';
import 'package:blagajna/models/company.dart';
import 'package:blagajna/models/employee.dart';

class Receipt {
  late Company company;
  late int receiptNumberInYear;
  late DateTime issuedAt;
  late Employee employee;
  late Iterable<BillEntryData> billEntries;

  Receipt(this.company, this.receiptNumberInYear, this.issuedAt, this.employee,
      this.billEntries);
}
