import 'package:blagajna/components/new_bill.dart';
import 'package:blagajna/models/company.dart';

class RegisterLockStatistics {
  final Company company;
  final int fromBill;
  final int toBill;
  final DateTime date;
  final double initialDeposit;
  final Iterable<BillEntryData> billEntries;
  final Map<String, double> totalByEmployee;

  RegisterLockStatistics(
      {required this.company,
      required this.fromBill,
      required this.toBill,
      required this.date,
      required this.initialDeposit,
      required this.billEntries,
      required this.totalByEmployee});
}
