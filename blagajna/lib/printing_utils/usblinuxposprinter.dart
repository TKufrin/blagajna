import 'dart:convert';
import 'dart:typed_data';

import 'package:blagajna/components/register_lock.dart';
import 'package:blagajna/printing_utils/printer.dart';
import 'package:blagajna/printing_utils/receipt.dart';
import 'package:blagajna/printing_utils/register_lock_statistics.dart';
import 'dart:io';
import 'package:intl/intl.dart';
import 'package:esc_pos_utils/esc_pos_utils.dart';
//import 'package:qr_flutter/qr_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:image/image.dart';

class UsbLinuxPosPrinter extends Printer {
  final String _usbPort;
  final String _linuxUsbDeviceLocation = "/dev/usb/";
  File? _usbStream;

  PaperSize _paperSize = PaperSize.mm58;

  UsbLinuxPosPrinter(this._usbPort) {
    _usbStream = File(_linuxUsbDeviceLocation + _usbPort);
    if (!_usbStream!.existsSync()) _usbStream = null;
  }

  set paperSize(PaperSize paperSize) {
    _paperSize = paperSize;
  }

  int _getMaxCharsPerLine(PosFontType? font) {
    if (_paperSize == PaperSize.mm58) {
      return (font == null || font == PosFontType.fontA) ? 32 : 42;
    } else {
      return (font == null || font == PosFontType.fontA) ? 48 : 64;
    }
  }

  @override
  Future<void> printReceipt(Receipt receipt) async {
    if (!isConnected()) {
      throw Error();
    }
    final profile = await CapabilityProfile.load();
    final generator = Generator(_paperSize, profile);
    List<int> bytes = [];

    bytes += generator.reset();
    bytes += generator.setGlobalCodeTable(
        'ISO_8859-2'); //support for Bosnian, Croatian and many other languages.

    bytes += generator.textEncoded(
        Uint8List.fromList(utf8.encode(receipt.company.name)),
        styles: const PosStyles(align: PosAlign.left));
    bytes += generator.textEncoded(
        Uint8List.fromList(utf8.encode(receipt.company.headquartersAddress)),
        styles: const PosStyles(align: PosAlign.left));
    bytes += generator.text("OIB: " + receipt.company.oib,
        styles: const PosStyles(align: PosAlign.left));

    bytes += generator.feed(1);

    bytes += generator.textEncoded(
        Uint8List.fromList(
            ("Racun br. " + receipt.receiptNumberInYear.toString()).codeUnits),
        styles: const PosStyles(align: PosAlign.center, bold: true));

    bytes += generator.feed(1);

    bytes += generator.text("Mjesto, datum i vrijeme",
        styles: const PosStyles(align: PosAlign.left));

    bytes += generator.hr();
    bytes += generator.textEncoded(
        Uint8List.fromList(receipt.company.officeAddress.codeUnits),
        styles: const PosStyles(align: PosAlign.left));
    bytes += generator.text("Datum i vrijeme:",
        styles: const PosStyles(align: PosAlign.left));
    bytes += generator.text(
        DateFormat('dd.MM.yyyy HH:mm:ss').format(receipt.issuedAt),
        styles: const PosStyles(align: PosAlign.left));

    bytes += generator.hr();
    bytes += generator.feed(1);

    /* bytes += generator.row([
      PosColumn(
        text: 'Artikal',
        width: 3,
        styles: const PosStyles(align: PosAlign.left),
      ),
      PosColumn(
        text: 'Kol',
        width: 3,
        styles: const PosStyles(align: PosAlign.left, bold: true),
      ),
      PosColumn(
        text: 'Popust',
        width: 3,
        styles: const PosStyles(align: PosAlign.left),
      ),
      PosColumn(
        text: 'Ukupno',
        width: 3,
        styles: const PosStyles(align: PosAlign.left),
      ),
    ]); */
    //na 58mm papiru 32 slova stanu u red
    int articleSpace = 13;
    int countSpace = 5;
    int discountSpace = 8;
    int totalSpace = 6;
    bytes += generator.text("Artikal      Kol  Popust  Ukupno");
    bytes += generator.hr();

    for (final billEntry in receipt.billEntries) {
      String name = _generateString(billEntry.articleDescription, articleSpace);
      String count =
          _generateString(billEntry.count.toStringAsFixed(2), countSpace);
      String discount = _generateString(
          billEntry.discount == null
              ? "0.00"
              : billEntry.discount!.toStringAsFixed(2),
          discountSpace);
      PosTextSize.size1;
      String total =
          _generateString(billEntry.total.toStringAsFixed(2), totalSpace);
      bytes += generator.text(name + count + discount + total,
          styles: const PosStyles(align: PosAlign.left));
      /* bytes += generator.row([
        PosColumn(
          text: entry.key,
          width: 3,
          styles: const PosStyles(align: PosAlign.left),
        ),
        PosColumn(
          text: entry.value.count.toStringAsFixed(2),
          width: 3,
          styles: const PosStyles(align: PosAlign.left),
        ),
        PosColumn(
          text: entry.value.tax == null
              ? "0.00"
              : entry.value.tax!.toStringAsFixed(2),
          width: 3,
          styles: const PosStyles(align: PosAlign.left),
        ),
        PosColumn(
          text: entry.value.rowSum.toStringAsFixed(2),
          width: 3,
          styles: const PosStyles(align: PosAlign.left),
        ),
      ]); */
    }
    bytes += generator.hr();

    String totalSum = receipt.billEntries
            .map((e) => e.total)
            .reduce((a, b) => a + b)
            .toStringAsFixed(2) +
        " kn";
    String ukupno =
        _generateString("Ukupno:", _getMaxCharsPerLine(null) - totalSum.length);
    bytes += generator.text(ukupno + totalSum,
        styles: const PosStyles(bold: true, align: PosAlign.left));
    String operaterId = receipt.employee.primaryKey!.toString();
    String operater = _generateString(
        "Operater:", _getMaxCharsPerLine(null) - operaterId.length);
    bytes += generator.text(operater + operaterId,
        styles: const PosStyles(align: PosAlign.left));

    bytes += generator.hr();

    Map<double, double> baseForPDV = getBaseForPDV(receipt.billEntries);

    int pdvSpace = 9;
    int baseSpace = 16;
    int outcomeSpace = 7;
    bytes += generator.text("PDV%     Osnovica        Iznos  ");
    for (final entry in baseForPDV.entries) {
      String pdv = _generateString(entry.key.toStringAsFixed(2), pdvSpace);
      String base = _generateString(entry.value.toStringAsFixed(2), baseSpace);
      String outcome = _generateString(
          (entry.value * entry.key / 100).toStringAsFixed(2), outcomeSpace);
      bytes += generator.text(pdv + base + outcome,
          styles: const PosStyles(align: PosAlign.left));
    }

    bytes += generator.hr();
    /* String qrData =
        "google.com"; //qr code should reflect link from porezna uprava
    const double qrSize = 200;
    try {
      final uiImg = await QrPainter(
        data: qrData,
        version: QrVersions.auto,
        gapless: false,
      ).toImageData(qrSize);
      final dir = await getTemporaryDirectory();
      final pathName = '${dir.path}/qr_tmp.png';
      final qrFile = File(pathName);
      final imgFile = await qrFile.writeAsBytes(uiImg!.buffer.asUint8List());
      final img = decodeImage(imgFile.readAsBytesSync());

      bytes += generator.image(img!);
    } catch (e) {
      print(e);
    } */
    bytes += generator.feed(5); //qrcode space
    bytes += generator.feed(1);
    bytes += generator.text("ZKI:");
    bytes += generator.text("JIR:");
    bytes += generator.hr();

    bytes += generator.text("Nacin placanja:        Novcanice");
    bytes += generator.hr();
    bytes += generator.text("HVALA NA POSJETI",
        styles: const PosStyles(bold: true, align: PosAlign.center));
    bytes += generator.hr();

    bytes += generator.cut();
    await _usbStream!.writeAsBytes(bytes);
  }

  @override
  void setReceiptPrintingConfiguration() {
    // TODO: implement setReceiptPrintingConfiguration
  }

  @override
  bool isConnected() {
    return _usbStream != null;
  }

  String _generateString(String text, int maxspace) {
    int spacesAfter = maxspace - text.length;
    for (int i = 0; i < spacesAfter; i++) {
      text += " ";
    }
    return text;
  }

  @override
  Future<void> printRegisterLock(RegisterLockStatistics statistics) async {
    if (!isConnected()) {
      throw Error();
    }
    final profile = await CapabilityProfile.load();
    final generator = Generator(_paperSize, profile);
    List<int> bytes = [];

    bytes += generator.reset();
    bytes += generator.setGlobalCodeTable(
        'ISO_8859-2'); //support for Bosnian, Croatian and many other languages.

    bytes += generator.textEncoded(
        Uint8List.fromList(utf8.encode(statistics.company.name)),
        styles: const PosStyles(align: PosAlign.left));
    bytes += generator.textEncoded(
        Uint8List.fromList(utf8.encode(statistics.company.headquartersAddress)),
        styles: const PosStyles(align: PosAlign.left));
    bytes += generator.text("OIB: " + statistics.company.oib,
        styles: const PosStyles(align: PosAlign.left));

    bytes += generator.feed(1);

    bytes += generator.text("Zakljucak kase na dan",
        styles: const PosStyles(align: PosAlign.center, bold: true));
    bytes += generator.text(DateFormat('dd.MM.yyyy').format(statistics.date),
        styles: const PosStyles(align: PosAlign.center, bold: true));

    bytes += generator.feed(1);

    bytes += generator.text("Od racuna: " + statistics.fromBill.toString(),
        styles: const PosStyles(align: PosAlign.left));
    bytes += generator.text("Do racuna: " + statistics.toBill.toString(),
        styles: const PosStyles(align: PosAlign.left));

    bytes += generator.text(
        "Polog kase: " + statistics.initialDeposit.toStringAsFixed(2),
        styles: const PosStyles(align: PosAlign.left));

    double iznos = getTotalWithoutDiscountApplied(statistics.billEntries);
    bytes += generator.text("Iznos: " + iznos.toStringAsFixed(2),
        styles: const PosStyles(align: PosAlign.left));

    double ukupno = getTotal(statistics.billEntries);

    bytes += generator.text(
        "Iznos popusta: " + (ukupno - iznos).toStringAsFixed(2),
        styles: const PosStyles(align: PosAlign.left));

    bytes += generator.feed(1);

    bytes += generator.text("PO DJELATNICIMA",
        styles: const PosStyles(align: PosAlign.left));

    bytes += generator.hr();

    for (final entry in statistics.totalByEmployee.entries) {
      bytes += generator.text(entry.key + ": " + entry.value.toStringAsFixed(2),
          styles: const PosStyles(align: PosAlign.left));
    }

    bytes += generator.hr();

    bytes += generator.text("UKUPNO: " + ukupno.toStringAsFixed(2),
        styles: const PosStyles(align: PosAlign.center, bold: true));

    bytes += generator.feed(1);

    bytes += generator.text("Porez po tarifnim skupinama",
        styles: const PosStyles(align: PosAlign.left));

    bytes += generator.hr();

    Map<double, double> baseForPDV = getBaseForPDV(statistics.billEntries);

    int pdvSpace = 9;
    int baseSpace = 16;
    int outcomeSpace = 7;
    bytes += generator.text("PDV%     Osnovica        Iznos  ");
    for (final entry in baseForPDV.entries) {
      String pdv = _generateString(entry.key.toStringAsFixed(2), pdvSpace);
      String base = _generateString(entry.value.toStringAsFixed(2), baseSpace);
      String outcome = _generateString(
          (entry.value * entry.key / 100).toStringAsFixed(2), outcomeSpace);
      bytes += generator.text(pdv + base + outcome,
          styles: const PosStyles(align: PosAlign.left));
    }

    bytes += generator.feed(1);

    bytes += generator.text(
        "Zavrsno stanje kase: " +
            (statistics.initialDeposit + ukupno).toStringAsFixed(2),
        styles: const PosStyles(align: PosAlign.left));

    bytes += generator.cut();
    await _usbStream!.writeAsBytes(bytes);
  }
}
