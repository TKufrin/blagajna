import 'package:blagajna/printing_utils/receipt.dart';
import 'package:blagajna/printing_utils/register_lock_statistics.dart';

abstract class Printer {
  Future<void> printReceipt(Receipt receipt);
  Future<void> printRegisterLock(RegisterLockStatistics statistics);
  bool isConnected();
  void setReceiptPrintingConfiguration();
}
