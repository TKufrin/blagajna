import 'package:blagajna/app_state.dart';
import 'package:blagajna/components/article_codebook.dart';
import 'package:blagajna/components/subcomponents/dialogs.dart';
import 'package:blagajna/components/text_styles/text_styles.dart';
import 'package:blagajna/models/article.dart';
import 'package:blagajna/models/notfoundexception.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class StockWidget extends StatefulWidget {
  const StockWidget({Key? key}) : super(key: key);

  @override
  State<StockWidget> createState() => _StockWidgetState();
}

class _StockWidgetState extends State<StockWidget> {
  late List<Article> _articles;

  Future<void> loadArticles() async {
    _articles = await Article().selectAll(orderBy: "code", limit: 50);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: loadArticles(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return _ArticleTableWidget(articles: _articles);
        } else {
          return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}

class _ArticleTableWidget extends StatefulWidget {
  const _ArticleTableWidget({Key? key, required this.articles})
      : super(key: key);

  final List<Article> articles;
  final int initalNumberOfArticles = 50;

  @override
  State<_ArticleTableWidget> createState() => __ArticleTableWidgetState();
}

class __ArticleTableWidgetState extends State<_ArticleTableWidget> {
  late final TextEditingController _codeController;
  late final TextEditingController _descriptionController;
  late final TextEditingController _numberFilterController;
  late List<Article> _articles;

  Future<void> filterArticles(int? _filteredCode, String _filteredDescription,
      [int? numArticlesToShow]) async {
    //when filtering we want to first query by order and only then filter
    String orderBy = "";
    switch (_sortedIndex) {
      case 0:
        orderBy += "code";
        break;
      case 1:
        orderBy += "description";
        break;
      case 3:
        orderBy += "unitPrice";
        break;
      default:
        throw Error();
    }
    if (_isAscending) {
      orderBy += " ASC";
    } else {
      orderBy += " DESC";
    }
    if (_filteredCode == null && _filteredDescription.isEmpty) {
      _articles =
          await Article().selectAll(orderBy: orderBy, limit: numArticlesToShow);
    } else if (_filteredCode == null) {
      _articles = await Article().selectArticlesByPrefixOfDescription(
          _filteredDescription,
          orderBy: orderBy,
          limit: numArticlesToShow);
    } else {
      Article art = Article();
      try {
        await art.loadArticleByCode(_filteredCode);
        _articles = [art];
      } on NotFoundException {
        _articles = [];
      }
    }

    setState(() {});
  }

  void _onSearchChange() {
    String code = _codeController.text.trim();
    String description = _descriptionController.text.trim();
    filterArticles(int.tryParse(code), description,
        int.tryParse(_numberFilterController.text));
  }

  @override
  void initState() {
    super.initState();
    _codeController = TextEditingController();
    _descriptionController = TextEditingController();
    _numberFilterController = TextEditingController();
    _numberFilterController.text = widget.initalNumberOfArticles.toString();
    _codeController.addListener(_onSearchChange);
    _descriptionController.addListener(_onSearchChange);
    _articles = widget.articles;
    if (widget.initalNumberOfArticles < _articles.length) {
      _articles = _articles.sublist(0, widget.initalNumberOfArticles);
    }
  }

  @override
  void dispose() {
    _codeController.removeListener(_onSearchChange);
    _descriptionController.removeListener(_onSearchChange);
    _codeController.dispose();
    _descriptionController.dispose();
    _numberFilterController.dispose();
    super.dispose();
  }

  int? _sortedIndex = 0;
  bool _isAscending = true;

  void onSort(int columnIndex, bool ascending) {
    if (columnIndex == 0) {
      setState(() {
        _sortedIndex = columnIndex;
        _isAscending = ascending;
        ascending
            ? _articles.sort((a, b) => a.code.compareTo(b.code))
            : _articles.sort((a, b) => b.code.compareTo(a.code));
      });
    } else if (columnIndex == 1) {
      setState(() {
        _sortedIndex = columnIndex;
        _isAscending = ascending;
        ascending
            ? _articles.sort((a, b) => a.description.compareTo(b.description))
            : _articles.sort((a, b) => b.description.compareTo(a.description));
      });
    } else if (columnIndex == 3) {
      setState(() {
        _sortedIndex = columnIndex;
        _isAscending = ascending;
        ascending
            ? _articles.sort((a, b) => a.unitPrice.compareTo(b.unitPrice))
            : _articles.sort((a, b) => b.unitPrice.compareTo(a.unitPrice));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    AppState appState = Provider.of<AppState>(context, listen: false);
    return Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
      Padding(
          padding: const EdgeInsets.only(bottom: 20),
          child: Row(children: [
            Padding(
              padding: const EdgeInsets.only(left: 40),
              child: ElevatedButton(
                  onPressed: () {
                    appState.setOnScreenWidget(
                        const ArticleCodebookWidget(
                          title: "Unos nove usluge",
                        ),
                        "Nova usluga");
                  },
                  child: Text(
                    "Novi unos",
                    style: font30,
                  )),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 40),
              child: ConstrainedBox(
                constraints: BoxConstraints.loose(const Size.fromWidth(200)),
                child: TextField(
                  style: font30,
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                  decoration: InputDecoration(
                    isDense: true,
                    labelText: "Broj zapisa:",
                    labelStyle: font30,
                  ),
                  controller: _numberFilterController,
                ),
              ),
            ),
            ElevatedButton(
                onPressed: () {
                  filterArticles(
                      int.tryParse(_codeController.text),
                      _descriptionController.text,
                      int.tryParse(_numberFilterController.text));
                },
                child: Text(
                  "Filtriraj",
                  style: font30,
                ))
          ])),
      Expanded(
        child: SingleChildScrollView(
          child: DataTable(
              sortColumnIndex: _sortedIndex,
              sortAscending: _isAscending,
              columnSpacing: 20,
              columns: [
                DataColumn(
                    onSort: onSort,
                    label: Expanded(
                      child: TextField(
                        style: font20,
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly
                        ],
                        decoration: InputDecoration(
                            labelText: "Šifra:",
                            labelStyle: font20,
                            isDense: true),
                        controller: _codeController,
                      ),
                    )),
                DataColumn(
                    onSort: onSort,
                    label: Expanded(
                      child: SizedBox(
                        width: 200,
                        child: TextField(
                          style: font20,
                          decoration: InputDecoration(
                            labelText: "Opis usluge:",
                            isDense: true,
                            labelStyle: font20,
                          ),
                          controller: _descriptionController,
                        ),
                      ),
                    )),
                DataColumn(
                    label: Expanded(
                  child: Text(
                    "Jedinica mjere",
                    style: font20,
                    textAlign: TextAlign.center,
                  ),
                )),
                DataColumn(
                    label: Expanded(
                      child: Text(
                        "Jedinična cijena (kn)",
                        style: font20,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    onSort: onSort),
                const DataColumn(label: Text("")),
                const DataColumn(label: Text(""))
              ],
              rows: _articles
                  .map((e) => DataRow(
                      cells: getDataRowFromArticle(
                          article: e,
                          onEditPressed: () => appState.setOnScreenWidget(
                              ArticleCodebookWidget(
                                initiallyLoadedArticle: e,
                                title: "Izmjena usluge",
                              ),
                              "Izmjena usluge"),
                          onDeletePressed: () async {
                            DialogAction? result = await showSimpleAlertDialog(
                                context,
                                cancelButtonText: "Odustani",
                                confirmButtonText: "Potvrdi",
                                contentText: "Potvrdite akciju");
                            if (result != null &&
                                result == DialogAction.confirm) {
                              await e.delete();
                              filterArticles(null, '');
                            }
                          })))
                  .toList()),
        ),
      ),
    ]);
  }
}

List<DataCell> getDataRowFromArticle(
    {required Article article,
    required VoidCallback onEditPressed,
    required VoidCallback onDeletePressed}) {
  return [
    DataCell(Center(
        child: Text(
      article.code.toString(),
      style: font20,
    ))),
    DataCell(Center(
      child: Text(
        article.description,
        style: font20,
      ),
    )),
    DataCell(Center(
        child: Text(
      article.unit.name,
      style: font20,
    ))),
    DataCell(Center(
        child: Text(
      article.unitPrice.toString(),
      style: font20,
    ))),
    DataCell(IconButton(
        onPressed: onEditPressed,
        iconSize: 30,
        icon: const Icon(
          Icons.edit,
          color: Colors.blue,
        ))),
    DataCell(IconButton(
      onPressed: onDeletePressed,
      iconSize: 30,
      icon: const Icon(Icons.delete),
      color: Colors.red,
    ))
  ];
}
