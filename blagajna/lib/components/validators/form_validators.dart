typedef VALIDATOR = String? Function(String?);

VALIDATOR getNameValidator(String warningMessage) {
  return (String? value) {
    if (value == null || value.isEmpty) {
      return warningMessage;
    }
    return null;
  };
}

VALIDATOR getOIBValidator() {
  return (String? value) {
    if (value == null || value.isEmpty) {
      return 'Unesite OIB';
    }

    value = value.trim();

    RegExp oibPattern = RegExp(r"^[0-9]{11}$");
    if (!oibPattern.hasMatch(value)) {
      return 'OIB mora sadrzavati 11 brojeva';
    }
    return null;
  };
}

VALIDATOR getRealNumberValidator(String warningMessage,
    {bool allowEmptyField = false}) {
  return (String? value) {
    if ((value == null || value.isEmpty) && allowEmptyField) return null;

    if (value == null || value.isEmpty) {
      return warningMessage;
    }
    value = value.trim();

    RegExp realNumberPattern = RegExp(r"^\d+(\.\d{1,2})*$");
    if (!realNumberPattern.hasMatch(value)) {
      return warningMessage;
    }
    return null;
  };
}

VALIDATOR getNaturalNumberValidator(String warningMessage,
    {bool allowEmptyField = false}) {
  return (String? value) {
    if ((value == null || value.isEmpty) && allowEmptyField) return null;
    if (value == null || value.isEmpty) {
      return warningMessage;
    }

    value = value.trim();

    RegExp naturalNumberPattern = RegExp(r"^[1-9][0-9]*$");
    if (!naturalNumberPattern.hasMatch(value)) {
      return warningMessage;
    }
    return null;
  };
}
