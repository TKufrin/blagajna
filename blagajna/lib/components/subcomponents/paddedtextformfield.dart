import 'package:blagajna/components/text_styles/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class PaddedTextFormField extends StatelessWidget {
  final String? Function(String?) validator;
  final String labelText;
  final TextEditingController? controller;
  final TextInputType? keyboardInput;
  final List<TextInputFormatter>? formatters;
  final bool filled;
  final Color? fillColor;
  final TextStyle style;
  final TextStyle labelStyle;
  final bool autofocus;
  final bool obscureText;

  const PaddedTextFormField(
      {Key? key,
      required this.labelText,
      required this.validator,
      this.controller,
      this.keyboardInput,
      this.formatters,
      this.filled = false,
      this.fillColor,
      this.style = const TextStyle(fontSize: 30),
      this.obscureText = false,
      this.labelStyle = const TextStyle(
          fontSize: 30, fontWeight: FontWeight.bold, color: Colors.black),
      this.autofocus = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: TextFormField(
        keyboardType: keyboardInput,
        inputFormatters: formatters,
        style: style,
        autofocus: autofocus,
        obscureText: obscureText,
        decoration: InputDecoration(
            border: const OutlineInputBorder(),
            isDense: true,
            filled: filled,
            fillColor: fillColor,
            labelText: labelText,
            labelStyle: labelStyle,
            errorStyle: font20colorRed),
        validator: validator,
        controller: controller,
      ),
    );
  }
}
