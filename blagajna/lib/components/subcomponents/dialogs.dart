import 'package:blagajna/components/text_styles/text_styles.dart';
import 'package:flutter/material.dart';

enum DialogAction { confirm, cancel }

Future<DialogAction?> showSimpleAlertDialog(BuildContext context,
    {String? cancelButtonText,
    required String confirmButtonText,
    required String contentText}) async {
  Widget? cancelButton;
  Widget confirmButton;
  List<Widget>? actions;

  if (cancelButtonText != null) {
    cancelButton = ElevatedButton(
      child: Text(
        cancelButtonText,
        style: font30,
      ),
      onPressed: () {
        Navigator.of(context).pop<DialogAction>(DialogAction.cancel);
      },
    );
    actions = [
      ...?actions,
      ...[cancelButton]
    ];
  }
  confirmButton = ElevatedButton(
    autofocus: true,
    child: Text(
      confirmButtonText,
      style: font30,
    ),
    onPressed: () {
      Navigator.of(context).pop<DialogAction>(DialogAction.confirm);
    },
  ); // set up the AlertDialog
  actions = [
    ...?actions,
    ...[confirmButton]
  ];
  // set up the buttons

  AlertDialog alert = AlertDialog(
      content: Text(
        contentText,
        style: font30,
      ),
      actions: actions); // show the dialog
  return await showDialog<DialogAction>(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
