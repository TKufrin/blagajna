import 'package:blagajna/app_state.dart';
import 'package:blagajna/components/backup_options.dart';
import 'package:blagajna/components/subcomponents/paddedtextformfield.dart';
import 'package:blagajna/components/text_styles/text_styles.dart';
import 'package:blagajna/components/validators/form_validators.dart';
import 'package:blagajna/models/company.dart';
import 'package:blagajna/models/gdrive_copy.dart';
import 'package:blagajna/models/local_copy.dart';
import 'package:blagajna/models/notfoundexception.dart';
import 'package:blagajna/models/print_settings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class SettingsWidget extends StatelessWidget {
  const SettingsWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 40),
      child: Row(crossAxisAlignment: CrossAxisAlignment.start, children: [
        const Expanded(
            child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 40),
                child: _ApplicationSettingsWidget())),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 40),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: const [
                Padding(
                    padding: EdgeInsets.only(bottom: 20),
                    child: _BackupSettingWidget()),
                _PosPrinterSettingsWidget(),
              ],
            ),
          ),
        )
      ]),
    );
  }
}

class _PosPrinterSettingsWidget extends StatefulWidget {
  const _PosPrinterSettingsWidget({Key? key}) : super(key: key);

  @override
  State<_PosPrinterSettingsWidget> createState() =>
      _PosPrinterSettingsWidgetState();
}

class _PosPrinterSettingsWidgetState extends State<_PosPrinterSettingsWidget> {
  final List<String> _lpPorts = ["lp1", "lp2", "lp3", "lp4", "lp5", "lp6"];
  late PrintSettings _settings;
  late String _selectedPort;

  @override
  void initState() {
    super.initState();
    _selectedPort = _lpPorts.first;
    _settings = PrintSettings();
    //async initialization but should fast enough
    loadSelectedPort();
  }

  Future<void> loadSelectedPort() async {
    try {
      await _settings.load(1);
      _selectedPort = _settings.printerPort;
    } on NotFoundException {
      //if not port configured, set default to lp1
      _selectedPort = _lpPorts.first;
      _settings.printerPort = _selectedPort;
      await _settings.updatePrintSettings();
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Padding(
          padding: const EdgeInsets.only(right: 20),
          child: Text("Mjesto ispisa računa:", style: font20)),
      DropdownButton<String>(
          value: _selectedPort,
          style: font20colorBlack,
          items: _lpPorts.map((String port) {
            return DropdownMenuItem<String>(value: port, child: Text(port));
          }).toList(),
          onChanged: (String? value) async {
            if (value == null) return;
            _selectedPort = value;
            _settings.printerPort = _selectedPort;
            await _settings.updatePrintSettings();
            setState(() {});
          }),
    ]);
  }
}

class _BackupSettingWidget extends StatelessWidget {
  const _BackupSettingWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AppState appState = Provider.of<AppState>(context, listen: false);
    return Row(children: [
      Text("Konfiguriraj postavke sustava sigurnosnih kopija:", style: font20),
      IconButton(
          iconSize: 40,
          onPressed: () async {
            appState.setOnScreenWidget(
                BackupOptionsWidget(
                  localCopies: await LocalCopy().selectAll(),
                  gdriveCopies: await GDriveCopy().selectAll(),
                ),
                "Sustav sigurnosnih kopija");
          },
          icon: const Icon(
            Icons.settings,
          ))
    ]);
  }
}

class _ApplicationSettingsWidget extends StatefulWidget {
  const _ApplicationSettingsWidget({Key? key}) : super(key: key);

  @override
  State<_ApplicationSettingsWidget> createState() =>
      __ApplicationSettingsWidgetState();
}

class __ApplicationSettingsWidgetState
    extends State<_ApplicationSettingsWidget> {
  final _formKey = GlobalKey<FormState>();

  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _officeAddressController =
      TextEditingController();
  final TextEditingController _oibController = TextEditingController();
  final TextEditingController _headquartersAddressController =
      TextEditingController();
  final TextEditingController _ownerController = TextEditingController();

  bool _successVisible = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _nameController.dispose();
    _officeAddressController.dispose();
    _oibController.dispose();
    _headquartersAddressController.dispose();
    _ownerController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    AppState appState = Provider.of<AppState>(context, listen: false);
    Company? company = appState.company;
    _nameController.text = company?.name ?? '';
    _officeAddressController.text = company?.officeAddress ?? '';
    _oibController.text = company?.oib ?? '';
    _headquartersAddressController.text = company?.headquartersAddress ?? '';
    _ownerController.text = company?.owner ?? '';
    return FocusScope(
      child: Form(
        key: _formKey,
        child: ListView(children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 20),
            child: Text(
              "Informacije o korisniku aplikacije",
              style: headlineStyle,
            ),
          ),
          PaddedTextFormField(
            autofocus: true,
            labelText: "Naziv tvrtke:",
            filled: true,
            validator: getNameValidator("Unesite naziv tvrtke"),
            controller: _nameController,
            style: font25,
            labelStyle: font25,
          ),
          PaddedTextFormField(
            labelText: "Adresa izdavača računa:",
            filled: true,
            validator: getNameValidator("Unesite adresu"),
            controller: _officeAddressController,
            style: font25,
            labelStyle: font25,
          ),
          PaddedTextFormField(
            labelText: "Sjedište tvrtke:",
            filled: true,
            validator: getNameValidator("Unesite sjedište"),
            controller: _headquartersAddressController,
            style: font25,
            labelStyle: font25,
          ),
          PaddedTextFormField(
            labelText: "OIB:",
            filled: true,
            formatters: [FilteringTextInputFormatter.digitsOnly],
            validator: getOIBValidator(),
            controller: _oibController,
            style: font25,
            labelStyle: font25,
          ),
          PaddedTextFormField(
            labelText: "Vlasnik:",
            filled: true,
            validator: getNameValidator("Unesite vlasnika"),
            controller: _ownerController,
            style: font25,
            labelStyle: font25,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20),
            child: ElevatedButton(
              onPressed: () async {
                if (_formKey.currentState!.validate()) {
                  Map<String, dynamic> data = {
                    "name": _nameController.text,
                    "officeAddress": _officeAddressController.text,
                    "headquartersAddress": _headquartersAddressController.text,
                    "oib": _oibController.text,
                    "owner": _ownerController.text,
                  };
                  Company newCompany = Company.withData(data);
                  appState.company = newCompany;

                  try {
                    await newCompany.updateCompanyData();
                    setState(() {
                      _successVisible = true;
                    });
                  } catch (ex) {
                    print("COMPANY INFO DIDN'T UPDATE PROPERLY");
                  }
                }
              },
              child: Text(
                'Spremi promjene',
                style: font30,
              ),
            ),
          ),
          AnimatedOpacity(
            duration: const Duration(milliseconds: 500),
            opacity: _successVisible ? 1.0 : 0.0,
            onEnd: () {
              if (_successVisible) {
                setState(() {
                  _successVisible = false;
                });
              }
            },
            child: Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Text(
                  "Pohranjeno!",
                  style: successStyle,
                )),
          ),
        ]),
      ),
    );
  }
}
