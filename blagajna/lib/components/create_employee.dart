import 'package:blagajna/app_state.dart';
import 'package:blagajna/components/new_bill.dart';
import 'package:blagajna/components/subcomponents/paddedtextformfield.dart';
import 'package:blagajna/components/text_styles/text_styles.dart';
import 'package:blagajna/components/utils/responsive_utils.dart';
import 'package:blagajna/components/validators/form_validators.dart';
import 'package:blagajna/main.dart';
import 'package:blagajna/models/employee.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CreateEmployeeWidget extends StatefulWidget {
  const CreateEmployeeWidget({
    Key? key,
    this.initiallyLoadedEmployee,
  }) : super(key: key);

  final Employee? initiallyLoadedEmployee;

  @override
  State<CreateEmployeeWidget> createState() => _CreateEmployeeWidgetState();
}

class _CreateEmployeeWidgetState extends State<CreateEmployeeWidget> {
  bool _showForm = true;
  bool _formPresent = true;

  void refresh() {
    setState(() {
      _formPresent = !_formPresent;
    });
  }

  @override
  Widget build(BuildContext context) {
    AppState appState = Provider.of(context, listen: false);
    if (_showForm) {
      return AnimatedOpacity(
          opacity: _formPresent ? 1.0 : 0.0,
          duration: const Duration(milliseconds: 500),
          onEnd: () {
            setState(() {
              _showForm = false;
            });
          },
          child: _CreateEmpoyeeFormWidget(
            notifyEmployeeAddedSuccessfully: refresh,
            initiallyLoadedEmployee: widget.initiallyLoadedEmployee,
          ));
    }
    return AnimatedOpacity(
        opacity: _showForm ? 0.0 : 1.0,
        duration: const Duration(milliseconds: 1000),
        onEnd: () {
          appState.setOnScreenWidget(null);
        },
        child: const Center(
          child: Text("Uspjeh!",
              style: TextStyle(
                  fontSize: 40,
                  color: Colors.green,
                  fontWeight: FontWeight.bold)),
        ));
  }
}

class _CreateEmpoyeeFormWidget extends StatefulWidget {
  final VoidCallback notifyEmployeeAddedSuccessfully;
  const _CreateEmpoyeeFormWidget({
    Key? key,
    required this.notifyEmployeeAddedSuccessfully,
    this.initiallyLoadedEmployee,
  }) : super(key: key);

  final Employee? initiallyLoadedEmployee;

  @override
  State<_CreateEmpoyeeFormWidget> createState() =>
      _CreateEmpoyeeFormWidgetState();
}

class _CreateEmpoyeeFormWidgetState extends State<_CreateEmpoyeeFormWidget> {
  final _formKey = GlobalKey<FormState>();
  bool _isAdmin = false;
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _surnameController = TextEditingController();
  final TextEditingController _oibController = TextEditingController();
  final TextEditingController _pinController = TextEditingController();
  bool _errorVisible = false;

  String headlineText = "Novi djelatnik";

  @override
  void initState() {
    super.initState();
    if (widget.initiallyLoadedEmployee != null) {
      headlineText = "Izmjena podataka djelatnika";
      _nameController.text = widget.initiallyLoadedEmployee!.name;
      _surnameController.text = widget.initiallyLoadedEmployee!.surname;
      _oibController.text = widget.initiallyLoadedEmployee!.oib;
      _pinController.text = widget.initiallyLoadedEmployee!.pin;
      _isAdmin = widget.initiallyLoadedEmployee!.admin == 1;
    }
  }

  @override
  void dispose() {
    _nameController.dispose();
    _surnameController.dispose();
    _oibController.dispose();
    _pinController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size formSize = get4By3FromHeight(context);
    return Center(
      child: ConstrainedBox(
        constraints: BoxConstraints.loose(formSize),
        child: Form(
          key: _formKey,
          child: FocusScope(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Text(
                  headlineText,
                  style: headlineStyle,
                ),
                PaddedTextFormField(
                  autofocus: true,
                  labelText: "Ime: ",
                  validator: getNameValidator("Unesite ime"),
                  controller: _nameController,
                ),
                PaddedTextFormField(
                  labelText: "Prezime: ",
                  validator: getNameValidator("Unesite prezime"),
                  controller: _surnameController,
                ),
                PaddedTextFormField(
                  labelText: "OIB: ",
                  validator: getOIBValidator(),
                  controller: _oibController,
                ),
                PaddedTextFormField(
                  labelText: "PIN: ",
                  obscureText: true,
                  validator: getNameValidator("Unesite PIN"),
                  controller: _pinController,
                ),
                Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: CheckboxListTile(
                      title: Text(
                        'Admin?',
                        style: font30,
                      ),
                      value: _isAdmin,
                      controlAffinity: ListTileControlAffinity.platform,
                      contentPadding: const EdgeInsets.only(left: 5),
                      onChanged: (bool? value) {
                        setState(() {
                          _isAdmin = value!;
                        });
                      },
                    )),
                Visibility(
                    visible: _errorVisible,
                    child: Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: Text("OIB ili PIN već postoje",
                            style: warningStyle))),
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: ElevatedButton(
                    onPressed: () async {
                      if (_formKey.currentState!.validate()) {
                        Map<String, dynamic> data = {
                          "name": _nameController.text,
                          "surname": _surnameController.text,
                          "pin": _pinController.text,
                          "oib": _oibController.text,
                          "admin": _isAdmin ? 1 : 0,
                        };
                        Employee newEmp = Employee.withData(data);
                        if (widget.initiallyLoadedEmployee == null) {
                          try {
                            await newEmp.insert();
                            widget.notifyEmployeeAddedSuccessfully();
                          } catch (ex) {
                            setState(() {
                              _errorVisible = true;
                            });
                          }
                        } else {
                          newEmp.primaryKey =
                              widget.initiallyLoadedEmployee!.primaryKey;
                          await newEmp.update();
                          AppState appState = context.read<AppState>();
                          //if we updated ourself than notify app bar of change
                          if (newEmp.primaryKey! ==
                              appState.employee!.primaryKey!) {
                            appState.employee = newEmp;
                          }
                          setEmployeeDisplayWidgetOnScreen(appState);
                        }
                      }
                    },
                    child: Text(
                      'Potvrdi',
                      style: font30,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: ElevatedButton(
                    onPressed: () {
                      AppState appState = context.read<AppState>();
                      if (widget.initiallyLoadedEmployee == null) {
                        appState.setOnScreenWidget(null);
                      } else {
                        setEmployeeDisplayWidgetOnScreen(appState);
                      }
                    },
                    style: ElevatedButton.styleFrom(primary: Colors.red),
                    child: Text(
                      'Odustani',
                      style: font30,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
