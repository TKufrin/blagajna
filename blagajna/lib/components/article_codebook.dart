import 'package:blagajna/app_state.dart';
import 'package:blagajna/components/stock.dart';
import 'package:blagajna/components/subcomponents/paddedtextformfield.dart';
import 'package:blagajna/components/text_styles/text_styles.dart';
import 'package:blagajna/components/utils/realnumbertexteditinghelper.dart';
import 'package:blagajna/components/utils/responsive_utils.dart';
import 'package:blagajna/components/validators/form_validators.dart';
import 'package:blagajna/models/article.dart';
import 'package:blagajna/models/enums/article_unit.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sqflite/sqflite.dart';
import 'package:provider/provider.dart';

class ArticleCodebookWidget extends StatefulWidget {
  const ArticleCodebookWidget(
      {Key? key, this.initiallyLoadedArticle, required this.title})
      : super(key: key);

  final Article? initiallyLoadedArticle;
  final String title;

  @override
  State<ArticleCodebookWidget> createState() => _ArticleCodebookWidgetState();
}

class _ArticleCodebookWidgetState extends State<ArticleCodebookWidget> {
  final _formKey = GlobalKey<FormState>();

  //final TextEditingController _barcodeController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  final TextEditingController _codeController = TextEditingController();
  final TextEditingController _unitPriceController = TextEditingController();
  final TextEditingController _discountController = TextEditingController();
  final TextEditingController _taxController = TextEditingController();
  //final TextEditingController _initialOnStockController =
  //TextEditingController();

  final List<ArticleUnit> _unitOptions = [
    ArticleUnit.noUnit,
    ArticleUnit.discrete,
    ArticleUnit.weightKg
  ];

  late ArticleUnit _selectedUnit;
  late String _affirmativeButtonText;
  final String _cancelButtonText = "Odustani";

  bool _resultingMessageVisible = false;
  String _resultingMessage = "Napravljen novi unos!";
  TextStyle _resultingMessageStyle = successStyle;

  void _editRealNumberFields() {
    realNumberTextEditingHelper(_taxController);
    realNumberTextEditingHelper(_discountController);
    realNumberTextEditingHelper(_unitPriceController);
  }

  @override
  void initState() {
    super.initState();
    _selectedUnit = _unitOptions.elementAt(0);
    _affirmativeButtonText = "Novi unos";
    if (widget.initiallyLoadedArticle != null) {
      _setTextFieldsFromLoadedArticle(widget.initiallyLoadedArticle!);
      _affirmativeButtonText = "Spremi promjene";
    }
    _taxController.addListener(_editRealNumberFields);
    _discountController.addListener(_editRealNumberFields);
    _unitPriceController.addListener(_editRealNumberFields);
  }

  @override
  void dispose() {
    _taxController.removeListener(_editRealNumberFields);
    _discountController.removeListener(_editRealNumberFields);
    _unitPriceController.removeListener(_editRealNumberFields);

    //_barcodeController.dispose();
    _descriptionController.dispose();
    _codeController.dispose();
    _unitPriceController.dispose();
    _discountController.dispose();
    _taxController.dispose();
    //_initialOnStockController.dispose();
    super.dispose();
  }

  void _setTextFieldsFromLoadedArticle(Article article) {
    _descriptionController.text = article.description;
    _taxController.text = article.tax?.toString() ?? '';
    _codeController.text = article.code.toString();
    _unitPriceController.text = article.unitPrice.toString();
    _discountController.text = article.discount?.toString() ?? '';
  }

  @override
  Widget build(BuildContext context) {
    Size formSize = getFormSizeForScreen(context);
    AppState appState = Provider.of<AppState>(context, listen: false);
    return Center(
      child: ConstrainedBox(
        constraints: BoxConstraints.tight(formSize),
        child: Row(children: [
          Expanded(
              flex: 4,
              child: Form(
                key: _formKey,
                child: FocusScope(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 20),
                          child: Text(
                            widget.title,
                            style: headlineStyle,
                          ),
                        ),
                        PaddedTextFormField(
                          keyboardInput: TextInputType.number,
                          formatters: [FilteringTextInputFormatter.digitsOnly],
                          labelText: "Šifra:",
                          validator: getNaturalNumberValidator(
                              "Unesite prirodan broj za šifru"),
                          controller: _codeController,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 20.0),
                          child: DropdownButtonFormField<ArticleUnit>(
                              style: font30colorBlack,
                              decoration: InputDecoration(
                                border: const OutlineInputBorder(),
                                isDense: true,
                                labelText: "Jedinica mjere",
                                labelStyle: formFieldLabelTextStyle,
                              ),
                              value: _selectedUnit,
                              items: _unitOptions.map((ArticleUnit unit) {
                                return DropdownMenuItem<ArticleUnit>(
                                    value: unit,
                                    child: Text(
                                      unit.name,
                                    ));
                              }).toList(),
                              onChanged: (ArticleUnit? value) {
                                if (value == null) return;
                                _selectedUnit = value;
                              }),
                        ),
                        PaddedTextFormField(
                          labelText: "Opis usluge:",
                          validator: getNameValidator("Unesite opis usluge"),
                          controller: _descriptionController,
                        ),
                        PaddedTextFormField(
                          keyboardInput: TextInputType.number,
                          formatters: [
                            FilteringTextInputFormatter.digitsOnly,
                          ],
                          labelText: "Jedinična cijena (kn):",
                          validator: getRealNumberValidator(
                              "Unesite broj sa najviše dvije decimale"),
                          controller: _unitPriceController,
                        ),
                        PaddedTextFormField(
                          keyboardInput: TextInputType.number,
                          formatters: [
                            FilteringTextInputFormatter.digitsOnly,
                          ],
                          labelText: "Porez (%):",
                          validator: getRealNumberValidator(
                              "Unesite broj sa najviše dvije decimale",
                              allowEmptyField: true),
                          controller: _taxController,
                        ),
                        PaddedTextFormField(
                          keyboardInput: TextInputType.number,
                          formatters: [
                            FilteringTextInputFormatter.digitsOnly,
                          ],
                          labelText: "Rabat (%):",
                          validator: getRealNumberValidator(
                              "Unesite broj sa najviše dvije decimale",
                              allowEmptyField: true),
                          controller: _discountController,
                        ),
                      ]),
                ),
              )),
          const Spacer(
            flex: 1,
          ),
          Expanded(
            flex: 2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                ElevatedButton(
                  onPressed: () async {
                    if (!_formKey.currentState!.validate()) return;
                    String tax = _taxController.text.trim();
                    String discount = _discountController.text.trim();
                    Map<String, dynamic> data = {
                      'description': _descriptionController.text.trim(),
                      'tax': tax.isEmpty ? null : double.parse(tax),
                      'discount':
                          discount.isEmpty ? null : double.parse(discount),
                      'unitPrice':
                          double.parse(_unitPriceController.text.trim()),
                      'unit': _selectedUnit.name.trim(),
                      'code': int.parse(_codeController.text.trim())
                    };
                    Article temp = Article.withData(data);
                    if (widget.initiallyLoadedArticle == null) {
                      // we are inserting a new article
                      try {
                        await temp.insert();
                        setState(() {
                          _resultingMessage = "Napravljen novi unos!";
                          _resultingMessageVisible = true;
                          _resultingMessageStyle = successStyle;
                        });
                      } on DatabaseException {
                        setState(() {
                          _resultingMessage =
                              "Šifra ili opis već postoje! Novi unos nemoguć.";
                          _resultingMessageVisible = true;
                          _resultingMessageStyle = warningStyle;
                        });
                      }
                    } else {
                      //identify the row to update
                      temp.primaryKey =
                          widget.initiallyLoadedArticle!.primaryKey;
                      int count = await temp.update();
                      if (count == 1) {
                        setState(() {
                          _resultingMessage = "Snimljene promjene!";
                          _resultingMessageVisible = true;
                          _resultingMessageStyle = successStyle;
                        });
                      } else {
                        setState(() {
                          _resultingMessage =
                              "Šifra ili opis već postoji, nemoguće snimit promjene!";
                          _resultingMessageVisible = true;
                          _resultingMessageStyle = warningStyle;
                        });
                      }
                    }
                  },
                  child: Text(
                    _affirmativeButtonText,
                    style: font30,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: ElevatedButton(
                    onPressed: () {
                      //we came from stock widget
                      appState.setOnScreenWidget(
                          const StockWidget(), "Pregled stanja");
                    },
                    style: ElevatedButton.styleFrom(primary: Colors.red),
                    child: Text(
                      _cancelButtonText,
                      style: font30,
                    ),
                  ),
                ),
                AnimatedOpacity(
                    duration: const Duration(milliseconds: 1000),
                    opacity: _resultingMessageVisible ? 1.0 : 0.0,
                    onEnd: () {
                      if (_resultingMessageStyle != warningStyle) {
                        // if warning we want to stay on the page
                        //we came from stock widget
                        appState.setOnScreenWidget(
                            const StockWidget(), "Pregled stanja");
                        return;
                      }
                      if (_resultingMessageVisible) {
                        setState(() {
                          _resultingMessageVisible = false;
                        });
                      }
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Text(_resultingMessage,
                          style: _resultingMessageStyle),
                    ))
              ],
            ),
          )
        ]),
      ),
    );
  }
}
