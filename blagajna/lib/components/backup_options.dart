import 'package:blagajna/backup_utils/google_drive_service.dart';
import 'package:blagajna/components/subcomponents/dialogs.dart';
import 'package:blagajna/components/text_styles/text_styles.dart';
import 'package:blagajna/database.dart';
import 'package:blagajna/models/abstract_copy.dart';
import 'package:blagajna/models/enums/copy_frequency.dart';
import 'package:blagajna/models/gdrive_copy.dart';
import 'package:blagajna/models/local_copy.dart';
import 'package:blagajna/models/notfoundexception.dart';
import 'package:flutter/material.dart';
import 'package:file_picker/file_picker.dart';

class BackupOptionsWidget extends StatelessWidget {
  const BackupOptionsWidget(
      {Key? key, required this.localCopies, required this.gdriveCopies})
      : super(key: key);

  final List<LocalCopy> localCopies;
  final List<GDriveCopy> gdriveCopies;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 40),
      child: Row(children: [
        Expanded(
            child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40),
          child: _LocalCopiesWidget(
            localCopies: localCopies,
          ),
        )),
        Expanded(
            child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40),
          child: _GoogleDriveCopiesWidget(
            gdriveCopies: gdriveCopies,
          ),
        )),
      ]),
    );
  }
}

class _LocalCopiesWidget extends StatefulWidget {
  const _LocalCopiesWidget({Key? key, required this.localCopies})
      : super(key: key);

  final List<LocalCopy> localCopies;

  @override
  State<_LocalCopiesWidget> createState() => _LocalCopiesWidgetState();
}

class _LocalCopiesWidgetState extends State<_LocalCopiesWidget> {
  late List<LocalCopy> _localCopies;

  @override
  void initState() {
    super.initState();
    _localCopies = widget.localCopies;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Text(
            "Postavke lokalnih kopija:",
            style: font25,
          ),
          ElevatedButton(
            onPressed: () async {
              String? selectedDirectory =
                  await FilePicker.platform.getDirectoryPath();
              if (selectedDirectory == null ||
                  selectedDirectory == await DatabaseHandler.getDbFolder()) {
                return;
              }
              LocalCopy lc = LocalCopy();
              try {
                await lc.loadLocalCopyByExistingPath(selectedDirectory);
                return; // this exists
              } on NotFoundException {
                lc = LocalCopy.withData(
                    {"path": selectedDirectory, "frequency": 0, "enabled": 0});
                lc.insert();
                setState(() {
                  _localCopies.insert(0, lc);
                });
              }
            },
            child: Text(
              "Nova putanja",
              style: font20,
            ),
          )
        ]),
        const Padding(
          padding: EdgeInsets.only(bottom: 20),
          child: Divider(
            color: Colors.black,
          ),
        ),
        Expanded(
          child: ListView.builder(
            itemBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.only(bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(_localCopies[index].path + ":", style: font25),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        _FrequencyOptionsWidget(copy: _localCopies[index]),
                        _EnableButtonWidget(copy: _localCopies[index]),
                        IconButton(
                          onPressed: () async {
                            DialogAction? result = await showSimpleAlertDialog(
                                context,
                                cancelButtonText: "Odustani",
                                confirmButtonText: "Potvrdi",
                                contentText: "Potvrdite akciju");
                            if (result != null &&
                                result == DialogAction.confirm) {
                              await _localCopies[index].delete();
                              setState(() {
                                _localCopies.removeAt(index);
                              });
                            }
                          },
                          iconSize: 30,
                          icon: const Icon(Icons.delete),
                          color: Colors.red,
                        )
                      ],
                    ),
                  ],
                ),
              );
            },
            itemCount: _localCopies.length,
          ),
        )
      ],
    );
  }
}

class _EnableButtonWidget extends StatefulWidget {
  const _EnableButtonWidget({Key? key, required this.copy}) : super(key: key);
  final AbstractCopy copy;

  @override
  State<_EnableButtonWidget> createState() => __EnableButtonWidgetState();
}

class __EnableButtonWidgetState extends State<_EnableButtonWidget> {
  late String _buttonText;
  late Color _buttonColor;
  late int _optionEnabled;

  @override
  void initState() {
    super.initState();
    _optionEnabled = widget.copy.enabled;
    _configureButtonBasedOnEnabled();
  }

  void _configureButtonBasedOnEnabled() {
    if (_optionEnabled == 1) {
      _buttonText = "Uključeno";
      _buttonColor = Colors.green;
    } else {
      _buttonText = "Isključeno";
      _buttonColor = Colors.red;
    }
  }

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        onPressed: () async {
          //flip option
          _optionEnabled = _optionEnabled == 1 ? 0 : 1;
          _configureButtonBasedOnEnabled();
          widget.copy.enabled = _optionEnabled;
          await widget.copy.update();
          setState(() {});
        },
        style: ElevatedButton.styleFrom(primary: _buttonColor),
        child: Text(
          _buttonText,
          style: font20,
        ));
  }
}

class _FrequencyOptionsWidget extends StatefulWidget {
  const _FrequencyOptionsWidget({Key? key, required this.copy})
      : super(key: key);

  final AbstractCopy copy;

  @override
  State<_FrequencyOptionsWidget> createState() =>
      __FrequencyOptionsWidgetState();
}

class __FrequencyOptionsWidgetState extends State<_FrequencyOptionsWidget> {
  late CopyFrequency _selectedOption;

  final List<CopyFrequency> frequencyOptions = [
    CopyFrequency.endOfWork,
    CopyFrequency.onReceiptIssue
  ];

  @override
  void initState() {
    super.initState();
    _selectedOption = widget.copy.frequency;
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButton<CopyFrequency>(
        style: font20colorBlack,
        value: _selectedOption,
        items: frequencyOptions.map((CopyFrequency option) {
          return DropdownMenuItem<CopyFrequency>(
              value: option, child: Text(option.name));
        }).toList(),
        onChanged: (CopyFrequency? value) async {
          if (value == null) return;
          _selectedOption = value;
          widget.copy.frequency = _selectedOption;
          await widget.copy.update();
          setState(() {});
        });
  }
}

class _GoogleDriveCopiesWidget extends StatefulWidget {
  const _GoogleDriveCopiesWidget({Key? key, required this.gdriveCopies})
      : super(key: key);

  final List<GDriveCopy> gdriveCopies;

  @override
  State<_GoogleDriveCopiesWidget> createState() =>
      _GoogleDriveCopiesWidgetState();
}

class _GoogleDriveCopiesWidgetState extends State<_GoogleDriveCopiesWidget> {
  late final List<GDriveCopy> _gdriveCopies;

  @override
  void initState() {
    super.initState();
    _gdriveCopies = widget.gdriveCopies;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Text(
            "Postavke Google Drive računa:",
            style: font25,
          ),
          ElevatedButton(
            onPressed: () async {
              String? uniqueName = await showDialog<String?>(
                  context: context,
                  builder: (context) => _GDriveUniqueNameDialog(
                      existingNames: _gdriveCopies.map((e) => e.uniqueName)));
              if (uniqueName == null) return;
              GoogleDriveService gs = GoogleDriveService();
              GDriveCopy newEntry = await gs.addNewGDriveAccount(uniqueName);
              await newEntry.insert();
              setState(() {
                _gdriveCopies.insert(0, newEntry);
              });
            },
            child: Text(
              "Novi račun",
              style: font20,
            ),
          )
        ]),
        const Padding(
          padding: EdgeInsets.only(bottom: 20),
          child: Divider(
            color: Colors.black,
          ),
        ),
        Expanded(
          child: ListView.builder(
            itemBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.only(bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text(
                      _gdriveCopies[index].uniqueName,
                      style: font25,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        _FrequencyOptionsWidget(copy: _gdriveCopies[index]),
                        _EnableButtonWidget(copy: _gdriveCopies[index]),
                        IconButton(
                          onPressed: () async {
                            DialogAction? result = await showSimpleAlertDialog(
                                context,
                                cancelButtonText: "Odustani",
                                confirmButtonText: "Potvrdi",
                                contentText: "Potvrdite akciju");
                            if (result != null &&
                                result == DialogAction.confirm) {
                              await _gdriveCopies[index].delete();
                              setState(() {
                                _gdriveCopies.removeAt(index);
                              });
                            }
                          },
                          iconSize: 30,
                          icon: const Icon(Icons.delete),
                          color: Colors.red,
                        )
                      ],
                    ),
                  ],
                ),
              );
            },
            itemCount: _gdriveCopies.length,
          ),
        )
      ],
    );
  }
}

class _GDriveUniqueNameDialog extends StatefulWidget {
  const _GDriveUniqueNameDialog({Key? key, required this.existingNames})
      : super(key: key);

  final Iterable<String> existingNames;

  @override
  State<_GDriveUniqueNameDialog> createState() =>
      _GDriveUniqueNameDialogState();
}

class _GDriveUniqueNameDialogState extends State<_GDriveUniqueNameDialog> {
  late final TextEditingController _nameController;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _nameController = TextEditingController();
  }

  @override
  void dispose() {
    _nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      title: const Center(
          child: Text("Jedinstveno ime za ovaj Google Drive račun")),
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Form(
            key: _formKey,
            child: TextFormField(
              autofocus: true,
              controller: _nameController,
              style: font30,
              validator: (String? value) {
                if (value == null || value.isEmpty) return "Unesite ime";
                if (widget.existingNames.contains(value.trim())) {
                  return "Ime već postoji";
                }
                return null;
              },
              onFieldSubmitted: (value) {
                if (!_formKey.currentState!.validate()) return;
                Navigator.pop<String?>(context, _nameController.text.trim());
              },
            ),
          ),
        ),
      ],
    );
  }
}
