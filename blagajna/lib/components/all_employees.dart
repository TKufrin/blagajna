import 'package:blagajna/app_state.dart';
import 'package:blagajna/components/create_employee.dart';
import 'package:blagajna/components/subcomponents/dialogs.dart';
import 'package:blagajna/components/text_styles/text_styles.dart';
import 'package:blagajna/models/employee.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class EmployeeDisplayWidget extends StatefulWidget {
  final List<Employee> employees;

  const EmployeeDisplayWidget({Key? key, required this.employees})
      : super(key: key);

  @override
  State<EmployeeDisplayWidget> createState() => _EmployeeDisplayWidgetState();
}

class _EmployeeDisplayWidgetState extends State<EmployeeDisplayWidget> {
  final TextStyle style = const TextStyle(
      fontSize: 30, fontWeight: FontWeight.bold, color: Colors.black);

  late List<Employee> employees;

  @override
  void initState() {
    super.initState();
    employees = widget.employees;
  }

  @override
  Widget build(BuildContext context) {
    AppState appState = Provider.of<AppState>(context, listen: false);
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(40),
        child: Column(children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 40),
            child: Text(
              "Djelatnici",
              style: headlineStyle,
            ),
          ),
          Expanded(
            child: Table(
              columnWidths: const {
                0: FractionColumnWidth(.1),
                1: FractionColumnWidth(.2),
                2: FractionColumnWidth(.2),
                3: FractionColumnWidth(.3),
                4: FractionColumnWidth(.1),
                5: FractionColumnWidth(.05),
                6: FractionColumnWidth(.05),
              },
              children: [
                TableRow(
                    decoration: BoxDecoration(
                        color: Colors.grey[200], border: Border.all()),
                    children: [
                      Container(
                        decoration: BoxDecoration(border: Border.all()),
                        alignment: Alignment.center,
                        child: Text(
                          "ID",
                          style: style,
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(border: Border.all()),
                        alignment: Alignment.center,
                        child: Text(
                          "IME",
                          style: style,
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(border: Border.all()),
                        alignment: Alignment.center,
                        child: Text(
                          "PREZIME",
                          style: style,
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(border: Border.all()),
                        alignment: Alignment.center,
                        child: Text(
                          "OIB",
                          style: style,
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(border: Border.all()),
                        alignment: Alignment.center,
                        child: Text(
                          "ADMIN",
                          style: style,
                        ),
                      ),
                      const Text(
                        "",
                      ),
                      const Text(
                        "",
                      ),
                    ]),
                ...employees.map((e) => getTableRowForEmployee(e,
                    onEditPressed: () => appState.setOnScreenWidget(
                        CreateEmployeeWidget(
                          initiallyLoadedEmployee: e,
                        ),
                        "Izmjena podataka o djelatniku"),
                    onDeletePressed:
                        appState.employee!.primaryKey! == e.primaryKey!
                            ? null
                            : () async {
                                DialogAction? result =
                                    await showSimpleAlertDialog(context,
                                        confirmButtonText: "Potvrdi",
                                        cancelButtonText: "Odustani",
                                        contentText: "Potvrdite akciju");
                                if (result != null &&
                                    result == DialogAction.confirm) {
                                  await e.delete();
                                  setState(() {
                                    employees.remove(e);
                                  });
                                }
                              }))
              ],
            ),
          ),
        ]),
      ),
    );
  }
}

TableRow getTableRowForEmployee(Employee e,
    {required VoidCallback onEditPressed,
    required VoidCallback? onDeletePressed}) {
  const TextStyle style = TextStyle(fontSize: 28, color: Colors.black);
  const double height = 40;
  return TableRow(children: [
    Container(
      height: height,
      decoration: BoxDecoration(border: Border.all()),
      alignment: Alignment.center,
      child: Text(
        e.primaryKey!.toString(),
        style: style,
      ),
    ),
    Container(
      height: height,
      decoration: BoxDecoration(border: Border.all()),
      alignment: Alignment.center,
      child: Text(
        e.name,
        style: style,
      ),
    ),
    Container(
      height: height,
      decoration: BoxDecoration(border: Border.all()),
      alignment: Alignment.center,
      child: Text(
        e.surname,
        style: style,
      ),
    ),
    Container(
      height: height,
      decoration: BoxDecoration(border: Border.all()),
      alignment: Alignment.center,
      child: Text(
        e.oib,
        style: style,
      ),
    ),
    StatefulBuilder(
      builder: (context, setState) => SizedBox(
        height: height,
        child: Transform.scale(
          scale: 1.3,
          child: Checkbox(
              value: e.admin == 1,
              onChanged: (bool? value) async {
                if (value == null) return;
                e.admin = value ? 1 : 0;
                await e.update();
                setState(() {});
              }),
        ),
      ),
    ),
    SizedBox(
      height: height,
      child: IconButton(
          onPressed: onEditPressed,
          iconSize: 30,
          icon: const Icon(
            Icons.edit,
            color: Colors.blue,
          )),
    ),
    SizedBox(
      height: height,
      child: IconButton(
        iconSize: 30,
        onPressed: onDeletePressed,
        icon: const Icon(Icons.delete),
        color: Colors.red,
      ),
    )
  ]);
}
