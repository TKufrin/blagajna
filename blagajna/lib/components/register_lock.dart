import 'dart:typed_data';

import 'package:blagajna/app_state.dart';
import 'package:blagajna/components/new_bill.dart';
import 'package:blagajna/components/subcomponents/dialogs.dart';
import 'package:blagajna/components/subcomponents/error.dart';
import 'package:blagajna/components/text_styles/text_styles.dart';
import 'package:blagajna/models/article.dart';
import 'package:blagajna/models/bill.dart';
import 'package:blagajna/models/bill_article.dart';
import 'package:blagajna/models/company.dart';
import 'package:blagajna/models/employee.dart';
import 'package:blagajna/models/notfoundexception.dart';
import 'package:blagajna/models/print_settings.dart';
import 'package:blagajna/printing_utils/printer.dart' as myprinter;
import 'package:blagajna/printing_utils/register_lock_statistics.dart';
import 'package:blagajna/printing_utils/usblinuxposprinter.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import 'package:screenshot/screenshot.dart';
import 'package:printing/printing.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;

class RegisterLockWidget extends StatefulWidget {
  const RegisterLockWidget({Key? key}) : super(key: key);

  @override
  State<RegisterLockWidget> createState() => _RegisterLockWidgetState();
}

class _RegisterLockWidgetState extends State<RegisterLockWidget> {
  DateTime _selectedDate = DateTime.now();

  late RegisterLockStatistics? _statistics;
  final ScreenshotController _screenshotController = ScreenshotController();

  void updateDate(DateTime date) {
    setState(() {
      _selectedDate = date;
    });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<void> _init(AppState appState) async {
    _statistics = await generateRegisterStatistics(
        appState.company!, _selectedDate, appState.initialDeposit!);
  }

  @override
  Widget build(BuildContext context) {
    AppState appState = Provider.of<AppState>(context, listen: false);
    return Center(
      child: Column(children: [
        Expanded(
          child: Screenshot(
            controller: _screenshotController,
            child: Padding(
              padding: const EdgeInsets.all(40),
              child: Column(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                        appState.company!.name,
                        style: font25,
                      ),
                      Text(
                        appState.company!.headquartersAddress,
                        style: font25,
                      ),
                      Text(
                        appState.company!.oib,
                        style: font25,
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Zaključak kase na dan: " +
                            DateFormat('dd.MM.yyyy').format(_selectedDate),
                        style: font30bold,
                      ),
                    ],
                  ),
                  const Divider(
                    color: Colors.black,
                  ),
                  Expanded(
                    child: FutureBuilder(
                      future: _init(appState),
                      builder: (context, snapshot) {
                        if (snapshot.connectionState == ConnectionState.done) {
                          if (_statistics == null) {
                            return const MyErrorWidget(
                                text: "Taj dan nije bilo izdanih računa!");
                          } else {
                            return Padding(
                              padding: const EdgeInsets.all(40),
                              child: _StatisticsShowingWidget(
                                  statistics: _statistics!),
                            );
                          }
                        } else {
                          return const CircularProgressIndicator();
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                width: 200,
                child: ElevatedButton(
                    onPressed: () async {
                      PrintSettings printSettings = PrintSettings();
                      try {
                        await printSettings.load(1);
                      } on NotFoundException {
                        await showSimpleAlertDialog(context,
                            contentText:
                                "U postavkama nije definirano mjesto ispisa",
                            confirmButtonText: "OK");
                        return;
                      }
                      final myprinter.Printer printer =
                          UsbLinuxPosPrinter(printSettings.printerPort);
                      if (!printer.isConnected()) {
                        await showSimpleAlertDialog(context,
                            contentText: "POS uređaj nije povezan",
                            confirmButtonText: "OK");
                        return;
                      }
                      await printer.printRegisterLock(_statistics!);
                    },
                    child: Text(
                      "Ispis - POS",
                      style: font25,
                    )),
              ),
              SizedBox(
                width: 200,
                child: ElevatedButton(
                    onPressed: () async {
                      double pixelRatio =
                          MediaQuery.of(context).devicePixelRatio;

                      Uint8List? image = await _screenshotController.capture(
                          pixelRatio: pixelRatio,
                          delay: const Duration(milliseconds: 1000));
                      if (image == null) return;

                      //await ShowCapturedWidget(context, image);
                      /* await Printing.layoutPdf(
                        onLayout: (_) => image,
                      ); */
                      await Printing.layoutPdf(
                        onLayout: (_) => _generatePdf(image),
                      );
                    },
                    child: Text(
                      "Ispis - A4",
                      style: font25,
                    )),
              ),
              SizedBox(
                width: 200,
                child: ElevatedButton(
                    onPressed: () async {
                      final DateTime? picked = await showDatePicker(
                          context: context,
                          initialDate: _selectedDate,
                          firstDate: DateTime(2015, 8),
                          lastDate: DateTime.now());
                      if (picked != null && picked != _selectedDate) {
                        updateDate(picked);
                      }
                    },
                    child: Text(
                      "Izaberi dan",
                      style: font25,
                    )),
              ),
            ],
          ),
        ),
      ]),
    );
  }
}

class _StatisticsShowingWidget extends StatelessWidget {
  const _StatisticsShowingWidget({Key? key, required this.statistics})
      : super(key: key);
  final RegisterLockStatistics statistics;

  @override
  Widget build(BuildContext context) {
    double totalWithoutDiscount =
        getTotalWithoutDiscountApplied(statistics.billEntries);
    double total = getTotal(statistics.billEntries);
    double totalDiscount = total - totalWithoutDiscount;

    return ListView(children: [
      Text(
        "Od računa: " + statistics.fromBill.toString(),
        style: font25,
      ),
      Text(
        "Do računa: " + statistics.toBill.toString(),
        style: font25,
      ),
      Text(
        "Polog kase: " + statistics.initialDeposit.toStringAsFixed(2) + "kn",
        style: font25,
      ),
      Text(
        "Iznos: " + totalWithoutDiscount.toStringAsFixed(2) + "kn",
        style: font25,
      ),
      Text(
        "Popust: " + totalDiscount.toStringAsFixed(2) + "kn",
        style: font25,
      ),
      const SizedBox(
        height: 20,
      ),
      Text(
        "Po djelatnicima",
        style: font25bold,
      ),
      const SizedBox(
        height: 10,
      ),
      ...statistics.totalByEmployee.entries.map((entry) => Text(
            entry.key + ": " + entry.value.toStringAsFixed(2) + "kn",
            style: font25,
          )),
      const SizedBox(
        height: 20,
      ),
      const Divider(
        color: Colors.black,
      ),
      Text(
        "UKUPNO: " + total.toStringAsFixed(2) + "kn",
        style: font30bold,
      ),
      const Divider(
        color: Colors.black,
      ),
      const SizedBox(
        height: 20,
      ),
      Text(
        "Porez po tarifnim skupinama",
        style: font25bold,
      ),
      const SizedBox(
        height: 10,
      ),
      Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Text(
          "PDV (%)",
          style: font25,
        ),
        Text(
          "Osnovica (kn)",
          style: font25,
        ),
        Text(
          "Iznos (kn)",
          style: font25,
        )
      ]),
      const Divider(
        color: Colors.black,
      ),
      ...getBaseForPDV(statistics.billEntries).entries.map((entry) =>
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Text(
              entry.key.toStringAsFixed(2),
              style: font25,
            ),
            Text(
              entry.value.toStringAsFixed(2),
              style: font25,
            ),
            Text(
              (entry.value * entry.key / 100).toStringAsFixed(2),
              style: font25,
            )
          ])),
      const SizedBox(
        height: 40,
      ),
      Text(
        "Završno stanje kase: " +
            (statistics.initialDeposit + getTotal(statistics.billEntries))
                .toStringAsFixed(2) +
            "kn",
        style: font25bold,
      ),
    ]);
  }
}

Future<RegisterLockStatistics?> generateRegisterStatistics(
    Company company, DateTime date, double initialDeposit) async {
  List<Bill> bills =
      await Bill().getBillsOnDate(date, orderBy: Bill().getPrimaryKeyName());
  if (bills.isEmpty) return null;

  List<BillEntryData> billEntries = [];
  Map<String, double> totalByEmployee = {};

  for (final bill in bills) {
    Employee emp = Employee();
    await emp.load(bill.idemployee);
    if (!totalByEmployee.containsKey(emp.name + " " + emp.surname)) {
      totalByEmployee[emp.name + " " + emp.surname] = 0;
    }
    totalByEmployee[emp.name + " " + emp.surname] =
        totalByEmployee[emp.name + " " + emp.surname]! + bill.sum;
    List<BillArticle> entries =
        await BillArticle().getByBillId(bill.primaryKey!);
    for (final entry in entries) {
      billEntries.add(BillEntryData({
        'count': entry.count,
        'unitPrice': entry.unitPrice,
        'discount': entry.discount,
        'tax': entry.tax
      }, Article()));
    }
  }
  return RegisterLockStatistics(
      company: company,
      fromBill: bills.first.primaryKey!,
      toBill: bills.last.primaryKey!,
      date: date,
      initialDeposit: initialDeposit,
      billEntries: billEntries,
      totalByEmployee: totalByEmployee);
}

Map<double, double> getBaseForPDV(Iterable<BillEntryData> billEntries) {
  Map<double, double> baseForPDV = {};
  for (final billEntry in billEntries) {
    double? tax = billEntry.tax;
    if (tax != null && tax != 0) {
      if (!baseForPDV.containsKey(tax)) {
        baseForPDV[tax] = 0;
      }
      baseForPDV[tax] =
          baseForPDV[tax]! + billEntry.unitPrice * billEntry.count;
    }
  }
  return baseForPDV;
}

double getTotalWithoutDiscountApplied(Iterable<BillEntryData> billEntries) {
  return billEntries
      .map((e) => e.tax == null
          ? e.unitPrice * e.count
          : e.count * e.unitPrice * 100 / (100 - e.tax!))
      .reduce((a, b) => a + b);
}

double getTotal(Iterable<BillEntryData> billEntries) {
  return billEntries.map((e) => e.total).reduce((a, b) => a + b);
}

/* Future<dynamic> ShowCapturedWidget(
    BuildContext context, Uint8List capturedImage) {
  return showDialog(
    useSafeArea: false,
    context: context,
    builder: (context) => Scaffold(
      appBar: AppBar(
        title: Text("Captured widget screenshot"),
      ),
      body: Center(
          child: capturedImage != null
              ? Image.memory(capturedImage)
              : Container()),
    ),
  );
} */

Future<Uint8List> _generatePdf(Uint8List image,
    {PdfPageFormat format = PdfPageFormat.a4}) async {
  final pdf = pw.Document(version: PdfVersion.pdf_1_5, compress: true);

  final memImage = pw.MemoryImage(image);

  pdf.addPage(
    pw.Page(
      pageFormat: format,
      build: (context) {
        return pw.Image(memImage);
      },
    ),
  );

  return pdf.save();
}
