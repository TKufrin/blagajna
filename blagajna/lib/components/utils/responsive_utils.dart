import 'package:flutter/widgets.dart';

Size getFormSizeForScreen(BuildContext context) {
  double height = MediaQuery.of(context).size.height / 1.5;
  double width = MediaQuery.of(context).size.width / 1.5;
  /* double scalingFactor = 0.3;
  double mediaLimit = MediaQuery.of(context).size.width * scalingFactor;
  if (mediaLimit < width) {
    width = mediaLimit;
  } */
  return Size(width, height);
}

Size getDialogSizeForScreen(BuildContext context) {
  double height = MediaQuery.of(context).size.height / 3;
  double width = MediaQuery.of(context).size.width / 3;
  /* double scalingFactor = 0.3;
  double mediaLimit = MediaQuery.of(context).size.width * scalingFactor;
  if (mediaLimit < width) {
    width = mediaLimit;
  } */
  return Size(width, height);
}

Size get4By3FromHeight(BuildContext context) {
  double height = MediaQuery.of(context).size.height;
  double width = 3 * height / 4;
  return Size(width, height);
}
