import 'package:flutter/widgets.dart';

///Formats controller text by inserting a dot before last two digits.
///Must be used with a text field that has formatter: FilteringTextInputFormatter.digitsOnly.
void realNumberTextEditingHelper(TextEditingController controller) {
  String text = controller.text;
  if (text.isEmpty || text.contains(RegExp(r"\."))) return;
  int i = 0;
  for (; i < text.length; i++) {
    if (text[i] != '0') break;
  }
  text = text.substring(i);
  if (text.isEmpty) return;
  if (text.length == 1) {
    controller.text = "00.0" + text;
    controller.selection = TextSelection.fromPosition(
        TextPosition(offset: controller.text.length));
  } else if (text.length == 2) {
    controller.text = "00." + text;
    controller.selection = TextSelection.fromPosition(
        TextPosition(offset: controller.text.length));
  } else {
    String lastTwoDigits = text.substring(text.length - 2);
    String theRest = text.substring(0, text.length - 2);
    if (theRest.length == 1) {
      controller.text = "0" + theRest + "." + lastTwoDigits;
    } else {
      controller.text = theRest + "." + lastTwoDigits;
    }
    controller.selection = TextSelection.fromPosition(
        TextPosition(offset: controller.text.length));
  }
}
