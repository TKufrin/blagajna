import 'package:blagajna/app_state.dart';
import 'package:blagajna/backup_utils/backup_service.dart';
import 'package:blagajna/components/subcomponents/dialogs.dart';
import 'package:blagajna/components/text_styles/text_styles.dart';
import 'package:blagajna/components/utils/realnumbertexteditinghelper.dart';
import 'package:blagajna/models/article.dart';
import 'package:blagajna/models/bill.dart';
import 'package:blagajna/models/bill_article.dart';
import 'package:blagajna/models/company.dart';
import 'package:blagajna/models/employee.dart';
import 'package:blagajna/models/enums/article_unit.dart';
import 'package:blagajna/models/notfoundexception.dart';
import 'package:blagajna/models/print_settings.dart';
import 'package:blagajna/printing_utils/printer.dart';
import 'package:blagajna/printing_utils/receipt.dart';
import 'package:blagajna/printing_utils/usblinuxposprinter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class TotalSumData {
  final double totalSum;
  final Color totalSumColor;
  TotalSumData(this.totalSum, this.totalSumColor);
}

Future<Bill> persistBill(
    Iterable<BillEntryData> billEntries, Employee employee) async {
  Bill newBill = Bill.withData({
    "idemployee": employee.primaryKey,
    "sum": _calculateTotalSum(billEntries),
  });
  await newBill.insert();
  for (final billEntry in billEntries) {
    BillArticle billArticle = BillArticle.withData({
      "idbill": newBill.primaryKey,
      "idarticle": billEntry.articlePrimaryKey,
      "count": billEntry.count,
      "tax": billEntry.tax,
      "discount": billEntry.discount,
      "unitPrice": billEntry.unitPrice
    });
    await billArticle.insert();
  }
  return newBill;
}

Future<Receipt> generateReceipt(Company company, Employee employee,
    Iterable<BillEntryData> billEntries, Bill bill, DateTime dateTime) async {
  return Receipt(company, await bill.getNumberOfBillsInCurrentYear(), dateTime,
      employee, billEntries);
}

double _calculateTotalSum(Iterable<BillEntryData> rowList) {
  if (rowList.isEmpty) {
    return 0;
  }
  Iterable<BillEntryData> validRows =
      rowList.where((element) => element.isValidEntry());
  if (validRows.isEmpty) return 0;
  return validRows.map<double>((e) => e.total).reduce((a, b) => a + b);
}

class NewBillWidget extends StatefulWidget {
  const NewBillWidget({Key? key}) : super(key: key);

  @override
  State<NewBillWidget> createState() => _NewBillWidgetState();
}

class _NewBillWidgetState extends State<NewBillWidget> {
  late final TextEditingController _searchController;
  final Map<int, _ArticleRowWidget> _addedRows = {};

  late final FocusNode _searchFocus;
  late final FocusNode _printFocus;
  final ValueNotifier<TotalSumData> _totalSumNotifier =
      ValueNotifier(TotalSumData(0, Colors.black));

  @override
  void initState() {
    super.initState();
    _searchController = TextEditingController();

    _printFocus = FocusNode();
    _searchFocus = FocusNode();
    _searchFocus.requestFocus();
  }

  @override
  void dispose() {
    _searchController.dispose();
    _searchFocus.dispose();
    super.dispose();
  }

  bool _allArticleRowsValid() {
    for (final rowWidget in _addedRows.values) {
      if (!rowWidget.billEntryObject.isValidEntry()) {
        return false;
      }
    }
    return true;
  }

  _ArticleRowWidget _generateArticleRowFromArticle(
      BillEntryData billEntryObject) {
    return _ArticleRowWidget(
      billEntryObject: billEntryObject,
      printFocus: _printFocus,
      onRowDataChanged: (int articleCode) {
        BillEntryData data = _addedRows[articleCode]!.billEntryObject;
        double totalSum = _calculateTotalSum(_addedRows.values
            .map<BillEntryData>((value) => value.billEntryObject));
        //row has error and we need to repaint to red color
        if (!data.isValidEntry()) {
          _totalSumNotifier.value = TotalSumData(totalSum, Colors.red);
        } else {
          //if no error then check if no other row has error and change color back to black
          Color sumColor = Colors.black;
          if (!_allArticleRowsValid()) {
            sumColor = Colors.red;
          }
          _totalSumNotifier.value = TotalSumData(totalSum, sumColor);
        }
      },
      removeArticleInParent: (int articleCode) async {
        DialogAction? result = await showSimpleAlertDialog(context,
            confirmButtonText: "Potvrdi",
            cancelButtonText: "Odustani",
            contentText: "Potvrdite akciju");
        if (result != null && result == DialogAction.confirm) {
          setState(() {
            _addedRows.remove(articleCode);
            Color sumColor = Colors.black;
            if (!_allArticleRowsValid()) {
              sumColor = Colors.red;
            }
            _totalSumNotifier.value = TotalSumData(
                _calculateTotalSum(_addedRows.values
                    .map<BillEntryData>((value) => value.billEntryObject)),
                sumColor);
            _searchFocus.requestFocus();
          });
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Column(children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 40),
          child: Row(children: [
            Expanded(
              flex: 10,
              child: TextField(
                focusNode: _searchFocus,
                style: font40,
                decoration: InputDecoration(
                  isDense: true,
                  contentPadding: const EdgeInsets.all(40),
                  border: const OutlineInputBorder(),
                  labelText: "Traži:",
                  labelStyle: formFieldLabelTextStyle,
                ),
                controller: _searchController,
                onSubmitted: (val) {
                  _printFocus.requestFocus();
                },
                onChanged: (String? value) async {
                  if (value == null || value.isEmpty) return;
                  List<Article> initialArticles = await Article()
                      .selectArticlesByPrefixOfDescription(value);
                  Article? selectedArticle = await showDialog<Article?>(
                    context: context,
                    builder: (context) => _SearchDialog(
                        initialArticles: initialArticles, initialSearch: value),
                  );
                  _ArticleRowWidget? row;
                  if (selectedArticle != null &&
                      !_addedRows.containsKey(selectedArticle.code)) {
                    double? count = await showDialog<double?>(
                        context: context,
                        builder: (context) => _ArticleCountDialog(
                            isCountRealNumber:
                                selectedArticle.unit != ArticleUnit.discrete));
                    if (count == null || count == 0) return;
                    Map<String, dynamic> map = {
                      'count': count,
                      'unitPrice': selectedArticle.unitPrice,
                      'discount': selectedArticle.discount,
                      'tax': selectedArticle.tax
                    };
                    BillEntryData rowDataObject =
                        BillEntryData(map, selectedArticle); // set row data
                    row = _generateArticleRowFromArticle(rowDataObject);
                    setState(() {
                      _addedRows[selectedArticle.code] = row!;
                      _totalSumNotifier.value = TotalSumData(
                          _calculateTotalSum(_addedRows.values
                              .map<BillEntryData>(
                                  (element) => element.billEntryObject)),
                          Colors.black);
                    });
                  }
                  _searchController.clear();
                },
              ),
            ),
            const Spacer(),
            ValueListenableBuilder<TotalSumData>(
              valueListenable: _totalSumNotifier,
              builder: (context, value, child) => Text(
                value.totalSum.toStringAsFixed(2) + "kn",
                style: TextStyle(fontSize: 60, color: value.totalSumColor),
              ),
            ),
          ]),
        ),
        const _NewBillHeader(),
        Expanded(
          child: Column(
            children: _addedRows.values.toList(),
          ),
        ),
        SizedBox(
          width: 400,
          height: 50,
          child: ElevatedButton(
              focusNode: _printFocus,
              onPressed: () async {
                if (!_allArticleRowsValid() || _addedRows.isEmpty) {
                  await showSimpleAlertDialog(context,
                      contentText:
                          "Postoji greška u unosu ili nema postavljenih artikala",
                      confirmButtonText: "OK");
                  return;
                }
                AppState appState = context.read<AppState>();
                Bill newBill = await persistBill(
                    _addedRows.values.map((e) => e.billEntryObject),
                    appState.employee!);

                BackupService bs = BackupService();
                //async so we can continue to work
                bs.persistOnReciptIssue();

                Company? company = appState.company;
                PrintSettings printSettings = PrintSettings();
                if (company == null) {
                  await showSimpleAlertDialog(context,
                      contentText:
                          "Podaci tvrtke nisu postavljeni, ne mogu napraviti ispis",
                      confirmButtonText: "OK");
                  setState(() {
                    _addedRows.clear();
                  });
                  return;
                }
                try {
                  await printSettings.load(1);
                } on NotFoundException {
                  await showSimpleAlertDialog(context,
                      contentText: "U postavkama nije definirano mjesto ispisa",
                      confirmButtonText: "OK");
                  setState(() {
                    _addedRows.clear();
                  });
                  return;
                }
                final Printer printer =
                    UsbLinuxPosPrinter(printSettings.printerPort);
                if (!printer.isConnected()) {
                  await showSimpleAlertDialog(context,
                      contentText: "POS uređaj nije povezan",
                      confirmButtonText: "OK");
                  setState(() {
                    _addedRows.clear();
                  });
                  return;
                }
                await printer.printReceipt(await generateReceipt(
                  appState.company!,
                  appState.employee!,
                  _addedRows.values.map((e) => e.billEntryObject),
                  newBill,
                  DateTime.now(),
                ));
                setState(() {
                  _addedRows.clear();
                });
              },
              child: Text(
                "Ispis",
                style: font40,
              )),
        )
      ]),
    );
  }
}

class BillEntryData {
  final Map<String, dynamic> _data;
  final Article _article;
  bool _valid = true;
  BillEntryData(this._data, this._article);

  double _calculateEntryTotal() {
    double ret = 0;
    //total cost will be x = unitPrice * (1-discount) * count / (1-tax)
    ret += unitPrice;
    ret *= count;
    if (discount != null) {
      ret *= (100 - discount!) / 100;
    }
    if (tax != null) {
      ret *= 100 / (100 - tax!);
    }
    return ret;
  }

  bool isValidEntry() {
    return _valid;
  }

  set valid(bool valid) => _valid = valid;

  set count(double count) => _data['count'] = count;
  set tax(double? tax) => _data['tax'] = tax;
  set discount(double? discount) => _data['discount'] = discount;
  set unitPrice(double unitPrice) => _data['unitPrice'] = unitPrice;

  double get count => _data['count'];
  double? get tax => _data['tax'];
  double? get discount => _data['discount'];
  double get unitPrice => _data['unitPrice'];

  double get total => _calculateEntryTotal();
  String get articleDescription => _article.description;
  ArticleUnit get articleUnit => _article.unit;
  int get articleCode => _article.code;
  int get articlePrimaryKey => _article.primaryKey!;
}

class _ArticleRowWidget extends StatefulWidget {
  const _ArticleRowWidget(
      {Key? key,
      required this.removeArticleInParent,
      required this.onRowDataChanged,
      required this.printFocus,
      required this.billEntryObject})
      : super(key: key);

  final Future<void> Function(int articleCode) removeArticleInParent;
  final void Function(int articleKey) onRowDataChanged;
  final FocusNode printFocus;
  final BillEntryData billEntryObject;

  @override
  State<_ArticleRowWidget> createState() => _ArticleRowWidgetState();
}

class _ArticleRowWidgetState extends State<_ArticleRowWidget> {
  late final TextEditingController _countController;
  late final TextEditingController _unitPriceController;
  late final TextEditingController _discountController;
  late final TextEditingController _taxController;

  late final FocusNode _taxFocus;
  late final FocusNode _discountFocus;
  late final FocusNode _unitPriceFocus;
  late final FocusNode _countFocus;

  late double _rowSum;
  Color _rowSumColor = Colors.black;

  void _taxListener() {
    realNumberTextEditingHelper(_taxController);
  }

  void _discountListener() {
    realNumberTextEditingHelper(_discountController);
  }

  void _countListener() {
    realNumberTextEditingHelper(_countController);
  }

  void _unitPriceListener() {
    realNumberTextEditingHelper(_unitPriceController);
  }

  @override
  void initState() {
    super.initState();
    _countController = TextEditingController();
    _unitPriceController = TextEditingController();
    _discountController = TextEditingController();
    _taxController = TextEditingController();

    _taxFocus = FocusNode();
    _taxFocus.addListener(() {
      if (_taxFocus.hasFocus) {
        _taxController.selection = TextSelection(
            baseOffset: 0, extentOffset: _taxController.text.length);
      }
    });

    _discountFocus = FocusNode();
    _discountFocus.addListener(() {
      if (_discountFocus.hasFocus) {
        _discountController.selection = TextSelection(
            baseOffset: 0, extentOffset: _discountController.text.length);
      }
    });

    _countFocus = FocusNode();
    _countFocus.addListener(() {
      if (_countFocus.hasFocus) {
        _countController.selection = TextSelection(
            baseOffset: 0, extentOffset: _countController.text.length);
      }
    });

    _unitPriceFocus = FocusNode();
    _unitPriceFocus.addListener(() {
      if (_unitPriceFocus.hasFocus) {
        _unitPriceController.selection = TextSelection(
            baseOffset: 0, extentOffset: _unitPriceController.text.length);
      }
    });

    _countController.text =
        widget.billEntryObject.articleUnit == ArticleUnit.discrete
            ? widget.billEntryObject.count.toStringAsFixed(0)
            : widget.billEntryObject.count.toStringAsFixed(2);
    _unitPriceController.text =
        widget.billEntryObject.unitPrice.toStringAsFixed(2);
    _discountController.text =
        widget.billEntryObject.discount?.toStringAsFixed(2) ?? '';
    _taxController.text = widget.billEntryObject.tax?.toStringAsFixed(2) ?? '';

    _taxController.addListener(_taxListener);
    _discountController.addListener(_discountListener);
    _unitPriceController.addListener(_unitPriceListener);

    if (widget.billEntryObject.articleUnit != ArticleUnit.discrete) {
      _countController.addListener(_countListener);
    }

    _rowSum = widget.billEntryObject.total;
  }

  @override
  void dispose() {
    _taxController.removeListener(_taxListener);
    _discountController.removeListener(_discountListener);
    _unitPriceController.removeListener(_unitPriceListener);
    _countController.removeListener(_countListener);

    _taxFocus.dispose();
    _countFocus.dispose();
    _unitPriceFocus.dispose();
    _discountFocus.dispose();

    _countController.dispose();
    _unitPriceController.dispose();
    _discountController.dispose();
    _taxController.dispose();
    super.dispose();
  }

  void _notifyParentThatSumChanged() {
    double? count = double.tryParse(_countController.text);
    double? unitPrice = double.tryParse(_unitPriceController.text);
    double? discount = double.tryParse(_discountController.text);
    double? tax = double.tryParse(_taxController.text);

    if (unitPrice == null || count == null || count == 0) {
      setState(() {
        _rowSum = 0;
        _rowSumColor = Colors.red;
      });
      //row data no longer valid
      widget.billEntryObject.valid = false;
      widget.onRowDataChanged(widget.billEntryObject.articleCode);
      return;
    }

    widget.billEntryObject.valid = true;
    widget.billEntryObject.count = count;
    widget.billEntryObject.tax = tax;
    widget.billEntryObject.discount = discount;
    widget.billEntryObject.unitPrice = unitPrice;

    setState(() {
      _rowSum = widget.billEntryObject.total;
      _rowSumColor = Colors.black;
    });

    widget.onRowDataChanged(widget.billEntryObject.articleCode);
  }

  @override
  Widget build(BuildContext context) {
    const TextStyle style = TextStyle(fontSize: 25);
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: ConstrainedBox(
        constraints: BoxConstraints.tight(const Size.fromHeight(40)),
        child: Row(
          children: [
            Expanded(
              flex: 2,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Text(
                  widget.billEntryObject.articleCode.toString(),
                  style: style,
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Text(
                  widget.billEntryObject.articleDescription,
                  style: style,
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Center(
                  child: ConstrainedBox(
                      constraints:
                          BoxConstraints.loose(const Size.fromWidth(70)),
                      child: TextField(
                        focusNode: _countFocus,
                        onSubmitted: (value) =>
                            widget.printFocus.requestFocus(),
                        enabled: widget.billEntryObject.articleUnit !=
                            ArticleUnit.noUnit,
                        inputFormatters: widget.billEntryObject.articleUnit ==
                                ArticleUnit.discrete
                            ? null
                            : [FilteringTextInputFormatter.digitsOnly],
                        decoration: const InputDecoration(
                            contentPadding: EdgeInsets.only(bottom: 10)),
                        controller: _countController,
                        style: style,
                        onChanged: (String? value) {
                          _notifyParentThatSumChanged();
                        },
                      ))),
            ),
            Expanded(
              flex: 3,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Text(
                  widget.billEntryObject.articleUnit.name,
                  style: style,
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: Center(
                  child: ConstrainedBox(
                      constraints:
                          BoxConstraints.loose(const Size.fromWidth(120)),
                      child: TextField(
                        focusNode: _unitPriceFocus,
                        onSubmitted: (value) =>
                            widget.printFocus.requestFocus(),
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly
                        ],
                        decoration: const InputDecoration(
                            contentPadding: EdgeInsets.only(bottom: 10)),
                        controller: _unitPriceController,
                        style: style,
                        onChanged: (String? value) {
                          _notifyParentThatSumChanged();
                        },
                      ))),
            ),
            Expanded(
              flex: 3,
              child: Center(
                  child: ConstrainedBox(
                      constraints:
                          BoxConstraints.loose(const Size.fromWidth(80)),
                      child: TextField(
                        focusNode: _discountFocus,
                        onSubmitted: (value) =>
                            widget.printFocus.requestFocus(),
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly
                        ],
                        decoration: const InputDecoration(
                            contentPadding: EdgeInsets.only(bottom: 10)),
                        controller: _discountController,
                        style: style,
                        onChanged: (String? value) {
                          _notifyParentThatSumChanged();
                        },
                      ))),
            ),
            Expanded(
              flex: 3,
              child: Center(
                  child: ConstrainedBox(
                      constraints:
                          BoxConstraints.loose(const Size.fromWidth(80)),
                      child: TextField(
                        onSubmitted: (value) =>
                            widget.printFocus.requestFocus(),
                        inputFormatters: [
                          FilteringTextInputFormatter.digitsOnly
                        ],
                        decoration: const InputDecoration(
                            contentPadding: EdgeInsets.only(bottom: 10)),
                        controller: _taxController,
                        focusNode: _taxFocus,
                        style: style,
                        onChanged: (String? value) {
                          _notifyParentThatSumChanged();
                        },
                      ))),
            ),
            Expanded(
              flex: 3,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Text(
                  _rowSum.toStringAsFixed(2),
                  style:
                      TextStyle(fontSize: style.fontSize, color: _rowSumColor),
                ),
              ),
            ),
            Expanded(
              child: IconButton(
                  onPressed: () {
                    widget.removeArticleInParent(
                        widget.billEntryObject.articleCode);
                  },
                  iconSize: 40,
                  icon: const Icon(
                    Icons.playlist_remove_sharp,
                    color: Colors.red,
                  )),
            ),
          ],
        ),
      ),
    );
  }
}

class _ArticleCountDialog extends StatefulWidget {
  const _ArticleCountDialog({Key? key, required this.isCountRealNumber})
      : super(key: key);

  final bool isCountRealNumber;

  @override
  State<_ArticleCountDialog> createState() => _ArticleCountDialogState();
}

class _ArticleCountDialogState extends State<_ArticleCountDialog> {
  late final TextEditingController _countController;

  void _editRealNumberFields() {
    realNumberTextEditingHelper(_countController);
  }

  @override
  void initState() {
    super.initState();
    _countController = TextEditingController();
    if (widget.isCountRealNumber) {
      _countController.text = "01.00";
      _countController.selection =
          const TextSelection(baseOffset: 0, extentOffset: 5);
      _countController.addListener(_editRealNumberFields);
    } else {
      _countController.text = "1";
      _countController.selection =
          const TextSelection(baseOffset: 0, extentOffset: 1);
    }
  }

  @override
  void dispose() {
    _countController.removeListener(_editRealNumberFields);
    _countController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      title: Center(
          child: Text(
        "Odaberite količinu",
        style: font40,
      )),
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: TextField(
            style: font40,
            autofocus: true,
            inputFormatters: widget.isCountRealNumber
                ? [FilteringTextInputFormatter.digitsOnly]
                : null,
            decoration: const InputDecoration(
                isDense: true,
                contentPadding: EdgeInsets.symmetric(vertical: 30)),
            controller: _countController,
            onSubmitted: (value) {
              Navigator.pop<double?>(
                  context, double.tryParse(_countController.text.trim()));
            },
          ),
        ),
      ],
    );
  }
}

class _SearchDialog extends StatefulWidget {
  const _SearchDialog(
      {Key? key, required this.initialArticles, required this.initialSearch})
      : super(key: key);

  final List<Article> initialArticles;
  final String initialSearch;

  @override
  State<_SearchDialog> createState() => __SearchDialogState();
}

class __SearchDialogState extends State<_SearchDialog> {
  late final TextEditingController _searchController;
  late List<Article> _filteredArticles;

  Future<void> _filterArticles(String description) async {
    _filteredArticles =
        await Article().selectArticlesByPrefixOfDescription(description);
    _filteredArticles.sort(
      (a, b) => a.description.compareTo(b.description),
    );
    setState(() {});
  }

  void searchListener() {
    String text = _searchController.text.trim();
    _filterArticles(text);
  }

  @override
  void initState() {
    super.initState();
    _searchController = TextEditingController();
    _searchController.text = widget.initialSearch;
    _searchController.addListener(searchListener);
    _filteredArticles = widget.initialArticles;
    _filteredArticles.sort(
      (a, b) => a.description.compareTo(b.description),
    );
  }

  @override
  void dispose() {
    _searchController.removeListener(searchListener);
    _searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SingleChildScrollView(
        child: SizedBox(
          height: 500,
          child: SimpleDialog(
              title: Focus(
                onKey: (node, event) {
                  if (event.logicalKey == LogicalKeyboardKey.arrowDown) {
                    node.nextFocus();
                    return KeyEventResult.handled;
                  }
                  return KeyEventResult.ignored;
                },
                child: TextField(
                  autofocus: true,
                  style: font40,
                  decoration: const InputDecoration(
                    isDense: true,
                    border: OutlineInputBorder(),
                    contentPadding: EdgeInsets.symmetric(vertical: 30),
                  ),
                  controller: _searchController,
                  onSubmitted: (value) {
                    Navigator.pop<Article?>(
                        context,
                        _filteredArticles.isEmpty
                            ? null
                            : _filteredArticles.first);
                  },
                ),
              ),
              children: _filteredArticles
                  .map((article) => SizedBox(
                        width: 400,
                        child: SimpleDialogOption(
                          onPressed: () =>
                              Navigator.pop<Article>(context, article),
                          child: Padding(
                              padding: const EdgeInsets.only(top: 10),
                              child: Center(
                                  child: Text(
                                article.description,
                                style: font30,
                              ))),
                        ),
                      ))
                  .toList()),
        ),
      ),
    );
  }
}

class _NewBillHeader extends StatelessWidget {
  const _NewBillHeader({Key? key}) : super(key: key);

  final TextStyle style =
      const TextStyle(fontWeight: FontWeight.bold, fontSize: 25);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints.loose(const Size.fromHeight(40)),
      child: Row(
        children: [
          Expanded(
              flex: 2,
              child: Center(
                  child: Text(
                "Šifra",
                style: style,
              ))),
          Expanded(
              flex: 3,
              child: Center(
                  child: Text(
                "Opis usluge",
                style: style,
              ))),
          Expanded(
              flex: 2,
              child: Center(
                  child: Text(
                "Kol",
                style: style,
              ))),
          Expanded(
              flex: 3,
              child: Center(
                  child: Text(
                "Jed. mjere",
                style: style,
              ))),
          Expanded(
              flex: 3,
              child: Center(
                  child: Text(
                "Jed. cijena",
                style: style,
              ))),
          Expanded(
              flex: 3,
              child: Center(
                  child: Text(
                "Rabat(%)",
                style: style,
              ))),
          Expanded(
              flex: 3,
              child: Center(
                  child: Text(
                "Porez(%)",
                style: style,
              ))),
          Expanded(
              flex: 3,
              child: Center(
                  child: Text(
                "Sveukupno",
                style: style,
              ))),
          const Expanded(
              child: Center(
                  child: Text(
            "",
          )))
        ],
      ),
    );
  }
}
