import 'package:flutter/material.dart';

TextStyle headlineStyle = const TextStyle(
    fontSize: 35,
    fontWeight: FontWeight.bold,
    color: Colors.black,
    decoration: TextDecoration.underline);

TextStyle formFieldLabelTextStyle = const TextStyle(
    fontSize: 30, fontWeight: FontWeight.bold, color: Colors.black);

TextStyle successStyle = const TextStyle(
    fontSize: 22, color: Colors.green, fontWeight: FontWeight.bold);

TextStyle warningStyle = const TextStyle(
    color: Colors.red, fontWeight: FontWeight.bold, fontSize: 22);

TextStyle font40 = const TextStyle(fontSize: 40);

TextStyle font30 = const TextStyle(fontSize: 30);
TextStyle font30bold =
    const TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
TextStyle font30colorBlack = const TextStyle(fontSize: 30, color: Colors.black);

TextStyle font25 = const TextStyle(fontSize: 25);
TextStyle font25bold =
    const TextStyle(fontSize: 25, fontWeight: FontWeight.bold);
TextStyle font25colorBlack = const TextStyle(fontSize: 25, color: Colors.black);

TextStyle font20 = const TextStyle(fontSize: 20);
TextStyle font20bold =
    const TextStyle(fontSize: 20, fontWeight: FontWeight.bold);
TextStyle font20colorBlack = const TextStyle(fontSize: 20, color: Colors.black);
TextStyle font20colorRed = const TextStyle(fontSize: 20, color: Colors.red);
