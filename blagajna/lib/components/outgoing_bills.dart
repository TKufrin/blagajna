import 'package:blagajna/app_state.dart';
import 'package:blagajna/components/new_bill.dart';
import 'package:blagajna/components/subcomponents/dialogs.dart';
import 'package:blagajna/components/text_styles/text_styles.dart';
import 'package:blagajna/models/article.dart';
import 'package:blagajna/models/bill.dart';
import 'package:blagajna/models/bill_article.dart';
import 'package:blagajna/models/company.dart';
import 'package:blagajna/models/notfoundexception.dart';
import 'package:blagajna/models/print_settings.dart';
import 'package:blagajna/printing_utils/printer.dart';
import 'package:blagajna/printing_utils/usblinuxposprinter.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class OutgoingBillsWidget extends StatefulWidget {
  const OutgoingBillsWidget({Key? key, required this.bills}) : super(key: key);

  final List<Bill> bills;

  @override
  State<OutgoingBillsWidget> createState() => _OutgoingBillsWidgetState();
}

class _OutgoingBillsWidgetState extends State<OutgoingBillsWidget> {
  final TextStyle style = const TextStyle(
      fontSize: 30, fontWeight: FontWeight.bold, color: Colors.black);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(40),
        child: Column(children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 40),
            child: Text(
              "Izlazni računi",
              style: headlineStyle,
            ),
          ),
          Expanded(
            child: Table(
              columnWidths: const {
                0: FractionColumnWidth(.2),
                1: FractionColumnWidth(.2),
                2: FractionColumnWidth(.2),
                3: FractionColumnWidth(.15),
                4: FractionColumnWidth(.2),
                5: FractionColumnWidth(.05),
              },
              children: [
                TableRow(
                    decoration: BoxDecoration(
                        color: Colors.grey[200], border: Border.all()),
                    children: [
                      Container(
                        decoration: BoxDecoration(border: Border.all()),
                        alignment: Alignment.center,
                        child: Text(
                          "Broj računa",
                          style: style,
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(border: Border.all()),
                        alignment: Alignment.center,
                        child: Text(
                          "Datum",
                          style: style,
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(border: Border.all()),
                        alignment: Alignment.center,
                        child: Text(
                          "Vrijeme",
                          style: style,
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(border: Border.all()),
                        alignment: Alignment.center,
                        child: Text(
                          "Iznos",
                          style: style,
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(border: Border.all()),
                        alignment: Alignment.center,
                        child: Text(
                          "Plaćanje",
                          style: style,
                        ),
                      ),
                      const Text("")
                    ]),
                ...widget.bills
                    .map((e) => getTableRowForBill(e, widget.bills, context))
              ],
            ),
          ),
        ]),
      ),
    );
  }
}

TableRow getTableRowForBill(
  Bill bill,
  List<Bill> allBills,
  BuildContext context,
) {
  const TextStyle style = TextStyle(fontSize: 30, color: Colors.black);
  const double height = 50;
  DateTime issuedAt = DateTime.fromMillisecondsSinceEpoch(bill.issuedAt * 1000);
  DateTime lowerBound = DateTime(issuedAt.year, issuedAt.month, issuedAt.day);
  DateTime upperBound = DateTime(issuedAt.year, issuedAt.month, issuedAt.day)
      .add(const Duration(days: 1))
      .subtract(const Duration(seconds: 1));
  List<Bill> billsInTheSameDay = allBills.where((element) {
    DateTime elIssuedAt =
        DateTime.fromMillisecondsSinceEpoch(element.issuedAt * 1000);
    return elIssuedAt.isAtSameMomentAs(lowerBound) ||
        elIssuedAt.isAtSameMomentAs(upperBound) ||
        (elIssuedAt.isAfter(lowerBound) && elIssuedAt.isBefore(upperBound));
  }).toList();
  int billNumberInDay =
      billsInTheSameDay.length - billsInTheSameDay.indexOf(bill);

  return TableRow(children: [
    Container(
      height: height,
      decoration: BoxDecoration(border: Border.all()),
      alignment: Alignment.center,
      child: Text(
        bill.primaryKey!.toString() + " - " + billNumberInDay.toString(),
        style: style,
      ),
    ),
    Container(
      height: height,
      decoration: BoxDecoration(border: Border.all()),
      alignment: Alignment.center,
      child: Text(
        DateFormat("dd.MM.yyyy").format(issuedAt),
        style: style,
      ),
    ),
    Container(
      height: height,
      decoration: BoxDecoration(border: Border.all()),
      alignment: Alignment.center,
      child: Text(
        DateFormat("HH:mm:ss").format(issuedAt),
        style: style,
      ),
    ),
    Container(
      height: height,
      decoration: BoxDecoration(border: Border.all()),
      alignment: Alignment.center,
      child: Text(
        bill.sum.toStringAsFixed(2),
        style: style,
      ),
    ),
    Container(
      height: height,
      decoration: BoxDecoration(border: Border.all()),
      alignment: Alignment.center,
      child: const Text(
        "Novčanice",
        style: style,
      ),
    ),
    // HERE IT IS...
    SizedBox(
      height: height,
      child: TableRowInkWell(
        onTap: () async {
          AppState appState = context.read<AppState>();

          Company? company = appState.company;
          PrintSettings printSettings = PrintSettings();
          if (company == null) {
            await showSimpleAlertDialog(context,
                contentText:
                    "Podaci tvrtke nisu postavljeni, ne mogu napraviti ispis",
                confirmButtonText: "OK");
            return;
          }
          try {
            await printSettings.load(1);
          } on NotFoundException {
            await showSimpleAlertDialog(context,
                contentText: "U postavkama nije definirano mjesto ispisa",
                confirmButtonText: "OK");
            return;
          }
          final Printer printer = UsbLinuxPosPrinter(printSettings.printerPort);
          if (!printer.isConnected()) {
            await showSimpleAlertDialog(context,
                contentText: "POS uređaj nije povezan",
                confirmButtonText: "OK");
            return;
          }
          List<BillArticle> billArticles =
              await BillArticle().getByBillId(bill.primaryKey!);
          List<BillEntryData> entryData = [];
          for (final item in billArticles) {
            Article art = Article();
            await art.load(item.idarticle); // could throw, what then????
            entryData.add(BillEntryData({
              'count': item.count,
              'unitPrice': item.unitPrice,
              'discount': item.discount,
              'tax': item.tax
            }, art));
          }

          await printer.printReceipt(await generateReceipt(appState.company!,
              appState.employee!, entryData, bill, issuedAt));
        },
        child: const Icon(
          Icons.print,
          color: Colors.blue,
          size: 30,
        ),
      ),
    ),
  ]);
}
