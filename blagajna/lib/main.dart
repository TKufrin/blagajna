import 'package:blagajna/components/all_employees.dart';
import 'package:blagajna/components/new_bill.dart';
import 'package:blagajna/components/create_employee.dart';
import 'package:blagajna/components/outgoing_bills.dart';
import 'package:blagajna/components/register_lock.dart';
import 'package:blagajna/components/settings.dart';
import 'package:blagajna/components/stock.dart';
import 'package:blagajna/components/subcomponents/error.dart';
import 'package:blagajna/models/bill.dart';
import 'package:blagajna/models/employee.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'screens/login.dart';
import 'package:provider/provider.dart';
import 'app_state.dart';
import 'package:window_manager/window_manager.dart';

const double windowWidth = 1280;
const double windowHeight = 1024;
const String windowTitle = "Blagajna";

Future<void> main() async {
  //setupWindow();
  WidgetsFlutterBinding.ensureInitialized();
  // Must add this line.
  await windowManager.ensureInitialized();

  WindowOptions windowOptions = const WindowOptions(
    //maximumSize: Size(windowWidth, windowHeight),
    //minimumSize: Size(windowWidth, windowHeight),
    title: windowTitle,
    center: true,
  );
  windowManager.waitUntilReadyToShow(windowOptions, () async {
    await windowManager.show();
    await windowManager.focus();
  });
  AppState appState = AppState();
  await appState.loadCompany();
  runApp(ChangeNotifierProvider(
    create: (context) => appState,
    child: const MyApp(),
  ));
}

void setNewBillWidgetOnScreen(AppState appState) {
  appState.setOnScreenWidget(const NewBillWidget(), "Novi račun");
}

void setStockWidgetOnScreen(AppState appState) {
  appState.setOnScreenWidget(const StockWidget(), "Pregled stanja");
}

Future<void> setOutgoingBillsWidgetOnScreen(AppState appState) async {
  List<Bill> bills =
      await Bill().selectAll(orderBy: Bill().getPrimaryKeyName() + " DESC");
  appState.setOnScreenWidget(
      OutgoingBillsWidget(bills: bills), "Izlazni računi");
}

Future<void> setEmployeeDisplayWidgetOnScreen(AppState appState) async {
  Employee? currEmp = appState.employee;
  if (currEmp!.admin != 1) {
    appState.setOnScreenWidget(
        const MyErrorWidget(text: "Nemate dovoljno privilegija!"));
  } else {
    List<Employee> employees = await (Employee().selectAll());
    appState.setOnScreenWidget(
        EmployeeDisplayWidget(employees: employees), "Pregled djelatnika");
  }
}

void setCreateEmployeeWidgetOnScreen(AppState appState) {
  Employee? currEmp = appState.employee;
  if (currEmp!.admin != 1) {
    appState.setOnScreenWidget(
        const MyErrorWidget(text: "Nemate dovoljno privilegija!"));
  } else {
    appState.setOnScreenWidget(const CreateEmployeeWidget(), "Novi djelatnik");
  }
}

void setRegisterLockWidgetOnScreen(AppState appState) {
  if (appState.company == null) {
    appState.setOnScreenWidget(
        const MyErrorWidget(text: "Niste unijeli informacije o tvrtki!"));
  } else {
    appState.setOnScreenWidget(const RegisterLockWidget(), "Zaključak kase");
  }
}

void setSettingsWidgetOnScreen(AppState appState) {
  appState.setOnScreenWidget(const SettingsWidget(), "Postavke");
}

class SwitchBodyWidgetIntent extends Intent {
  const SwitchBodyWidgetIntent(this.widgetNumber, this.appState);
  final int widgetNumber;
  final AppState appState;
}

class SwitchWidgetBodyAction extends Action<SwitchBodyWidgetIntent> {
  @override
  void invoke(covariant SwitchBodyWidgetIntent intent) {
    switch (intent.widgetNumber) {
      case 1:
        setNewBillWidgetOnScreen(intent.appState);
        break;
      case 2:
        setStockWidgetOnScreen(intent.appState);
        break;
      case 3:
        setOutgoingBillsWidgetOnScreen(intent.appState);
        break;
      case 4:
        setEmployeeDisplayWidgetOnScreen(intent.appState);
        break;
      case 5:
        setCreateEmployeeWidgetOnScreen(intent.appState);
        break;
      case 6:
        setRegisterLockWidgetOnScreen(intent.appState);
        break;
      case 7:
        setSettingsWidgetOnScreen(intent.appState);
        break;
      default:
        return;
    }
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: windowTitle,
        theme: ThemeData(primarySwatch: Colors.green),
        debugShowCheckedModeBanner: false,
        shortcuts: <ShortcutActivator, Intent>{
          LogicalKeySet(
            LogicalKeyboardKey.f1,
          ): SwitchBodyWidgetIntent(1, context.read<AppState>()),
          LogicalKeySet(
            LogicalKeyboardKey.f2,
          ): SwitchBodyWidgetIntent(2, context.read<AppState>()),
          LogicalKeySet(
            LogicalKeyboardKey.f3,
          ): SwitchBodyWidgetIntent(3, context.read<AppState>()),
          LogicalKeySet(
            LogicalKeyboardKey.f4,
          ): SwitchBodyWidgetIntent(4, context.read<AppState>()),
          LogicalKeySet(
            LogicalKeyboardKey.f5,
          ): SwitchBodyWidgetIntent(5, context.read<AppState>()),
          LogicalKeySet(
            LogicalKeyboardKey.f6,
          ): SwitchBodyWidgetIntent(6, context.read<AppState>()),
          LogicalKeySet(
            LogicalKeyboardKey.f7,
          ): SwitchBodyWidgetIntent(7, context.read<AppState>()),
          ...WidgetsApp.defaultShortcuts
        },
        actions: <Type, Action<Intent>>{
          ...WidgetsApp.defaultActions,
          SwitchBodyWidgetIntent: SwitchWidgetBodyAction()
        },
        home: LoginWidget());
  }
}
